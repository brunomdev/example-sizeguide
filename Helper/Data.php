<?php

namespace Example\SizeGuide\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 *
 * @category Helper
 * @package  Example\SizeGuide\Helper
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Data extends AbstractHelper
{

}
