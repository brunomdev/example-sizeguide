<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\SizePivotInterface;
use Example\SizeGuide\Api\Data\SizePivotSearchResultsInterface;
use Example\SizeGuide\Api\SizePivotRepositoryInterface;
use Example\SizeGuide\Api\Data\SizePivotSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Example\SizeGuide\Model\Resource\SizePivot as SizePivotResource;
use Example\SizeGuide\Model\Resource\SizePivot\CollectionFactory as SizePivotCollectionFactory;

/**
 * Class SizePivotRepository
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizePivotRepository implements SizePivotRepositoryInterface
{
    /**
     * Size Pivot Resource
     *
     * @var SizePivotResource
     */
    protected $resource;

    /**
     * Size Pivot Factory
     *
     * @var SizePivotFactory
     */
    protected $sizePivotFactory;

    /**
     * Size Pivot Collection Factory
     *
     * @var SizePivotCollectionFactory
     */
    protected $sizePivotCollectionFactory;

    /**
     * Measure Search Results Factory
     *
     * @var SizePivotSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * SizePivotRepository constructor.
     *
     * @param SizePivotResource                      $resource                   Resource
     * @param SizePivotFactory                       $sizePivotFactory           Factory
     * @param SizePivotCollectionFactory             $sizePivotCollectionFactory Collection
     * @param SizePivotSearchResultsInterfaceFactory $searchResultsFactory       Results
     */
    public function __construct(
        SizePivotResource $resource,
        SizePivotFactory $sizePivotFactory,
        SizePivotCollectionFactory $sizePivotCollectionFactory,
        SizePivotSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->sizePivotFactory = $sizePivotFactory;
        $this->sizePivotCollectionFactory = $sizePivotCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @param SizePivot|SizePivotInterface $sizePivot Size Pivot Object
     *
     * @return SizePivotInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(SizePivotInterface $sizePivot)
    {
        try {
            $this->resource->save($sizePivot);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save the measure: %1',
                    $exception->getMessage()
                )
            );
        }

        return $sizePivot;
    }

    /**
     * Get a Size Pivot by ID
     *
     * @param string $sizePivotId Size Pivot ID
     *
     * @return SizePivot|SizePivotInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById($sizePivotId)
    {
        /* @var $sizePivot SizePivot */
        $sizePivot = $this->sizePivotFactory->create();
        $this->resource->load($sizePivot, $sizePivotId);

        if (!$sizePivot->getId()) {
            throw new NoSuchEntityException(
                __('Size with id %1 does not exist.', $sizePivotId)
            );
        }

        return $sizePivot;
    }

    /**
     * {@inheritDoc}
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return SizePivotSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->sizePivotCollectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }

        $sortOrders = $searchCriteria->getSortOrders();

        if ($sortOrders) {
            /* @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    (
                        $sortOrder->getDirection() == SortOrder::SORT_ASC
                    ) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * {@inheritDoc}
     *
     * @param SizePivot|SizePivotInterface $sizePivot Size Pivot
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException
     */
    public function delete(SizePivotInterface $sizePivot)
    {
        try {
            $this->resource->delete($sizePivot);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the Measure: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $sizePivotId Size Pivot ID
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException|LocalizedException|NoSuchEntityException
     */
    public function deleteById($sizePivotId)
    {
        return $this->delete($this->getById($sizePivotId));
    }

}
