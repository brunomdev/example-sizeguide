<?php

namespace Example\SizeGuide\Model\Entity\Attribute\Backend;

use Example\SizeGuide\Model\Resource\SizeGuide\CollectionFactory;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class SizeGuide
 *
 * @category Model
 * @package  Example\SizeGuide\Model\Entity\Attribute\Backend
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeGuide extends AbstractSource
{
    /**
     * SizeGuide Collection Factory
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * SizeGuide constructor.
     *
     * @param CollectionFactory $collectionFactory SizeGuide Collection Factory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $result[] = ['value' => '', 'label' => __(' ')];

            $sizeGuides = $this->collectionFactory->create();

            if ($sizeGuides->count() >= 1) {
                foreach ($sizeGuides as $sizeGuide) {
                    $result[] = [
                        'value' => $sizeGuide->getId(),
                        'label' => $sizeGuide->getName()
                    ];
                }
            }

            $this->_options = $result;
        }

        return $this->_options;
    }

}
