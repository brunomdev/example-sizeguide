<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\MeasurePivotInterface;
use Example\SizeGuide\Api\Data\MeasurePivotSearchResultsInterface;
use Example\SizeGuide\Api\MeasurePivotRepositoryInterface;
use Example\SizeGuide\Api\Data\MeasurePivotSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Example\SizeGuide\Model\Resource\MeasurePivot as MeasurePivotResource;
use Example\SizeGuide\Model\Resource\MeasurePivot\CollectionFactory as MeasurePivotCollectionFactory;

/**
 * Class MeasurePivotRepository
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasurePivotRepository implements MeasurePivotRepositoryInterface
{
    /**
     * Measure Pivot Resource
     *
     * @var MeasurePivotResource
     */
    protected $resource;

    /**
     * Measure Pivot Factory
     *
     * @var MeasurePivotFactory
     */
    protected $measurePivotFactory;

    /**
     * Measure Pivot Collection Factory
     *
     * @var MeasurePivotCollectionFactory
     */
    protected $measurePivotCollectionFactory;

    /**
     * Measure Search Results Factory
     *
     * @var MeasurePivotSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * MeasurePivotRepository constructor.
     *
     * @param MeasurePivotResource                      $resource                      Resource
     * @param MeasurePivotFactory                       $measurePivotFactory           Factory
     * @param MeasurePivotCollectionFactory             $measurePivotCollectionFactory Collection
     * @param MeasurePivotSearchResultsInterfaceFactory $searchResultsFactory          Results
     */
    public function __construct(
        MeasurePivotResource $resource,
        MeasurePivotFactory $measurePivotFactory,
        MeasurePivotCollectionFactory $measurePivotCollectionFactory,
        MeasurePivotSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->measurePivotFactory = $measurePivotFactory;
        $this->measurePivotCollectionFactory = $measurePivotCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @param MeasurePivot|MeasurePivotInterface $measurePivot Measure Pivot Object
     *
     * @return MeasurePivotInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(MeasurePivotInterface $measurePivot)
    {
        try {
            $this->resource->save($measurePivot);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save the measure: %1',
                    $exception->getMessage()
                )
            );
        }

        return $measurePivot;
    }

    /**
     * Get a Measure Pivot by ID
     *
     * @param string $measurePivotId Measure Pivot ID
     *
     * @return MeasurePivot|MeasurePivotInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById($measurePivotId)
    {
        /* @var $measurePivot MeasurePivot */
        $measurePivot = $this->measurePivotFactory->create();
        $this->resource->load($measurePivot, $measurePivotId);

        if (!$measurePivot->getId()) {
            throw new NoSuchEntityException(
                __('Measure with id %1 does not exist.', $measurePivotId)
            );
        }

        return $measurePivot;
    }

    /**
     * {@inheritDoc}
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return MeasurePivotSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->measurePivotCollectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }

        $sortOrders = $searchCriteria->getSortOrders();

        if ($sortOrders) {
            /* @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    (
                        $sortOrder->getDirection() == SortOrder::SORT_ASC
                    ) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * {@inheritDoc}
     *
     * @param MeasurePivot|MeasurePivotInterface $measurePivot Measure Pivot
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException
     */
    public function delete(MeasurePivotInterface $measurePivot)
    {
        try {
            $this->resource->delete($measurePivot);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the Measure: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $measurePivotId Measure Pivot ID
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException|LocalizedException|NoSuchEntityException
     */
    public function deleteById($measurePivotId)
    {
        return $this->delete($this->getById($measurePivotId));
    }

}
