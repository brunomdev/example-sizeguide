<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\SizeInterface;
use Example\SizeGuide\Api\Data\SizeSearchResultsInterface;
use Example\SizeGuide\Api\SizeRepositoryInterface;
use Example\SizeGuide\Api\Data\SizeSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Example\SizeGuide\Model\Resource\Size as SizeResource;
use Example\SizeGuide\Model\Resource\Size\CollectionFactory as SizeCollectionFactory;
use Example\SizeGuide\Model\Resource\SizePivot\CollectionFactory as SizePivotCollectionFactory;

/**
 * Class SizeRepository
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeRepository implements SizeRepositoryInterface
{
    /**
     * Size Resource
     *
     * @var SizeResource
     */
    protected $resource;

    /**
     * Size Factory
     *
     * @var SizeFactory
     */
    protected $sizeFactory;

    /**
     * Size Collection Factory
     *
     * @var SizeCollectionFactory
     */
    protected $sizeCollectionFactory;

    /**
     * Size Pivot Collection Factory
     *
     * @var SizePivotCollectionFactory
     */
    protected $sizePivotCollectionFactory;

    /**
     * Size Search Results Factory
     *
     * @var SizeSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * SizeRepository constructor.
     *
     * @param SizeResource                      $resource                   Size Resource
     * @param SizeFactory                       $sizeFactory                Size Factory
     * @param SizeCollectionFactory             $sizeCollectionFactory      SizeCollection
     * @param SizePivotCollectionFactory        $sizePivotCollectionFactory SizePivotCollection
     * @param SizeSearchResultsInterfaceFactory $searchResultsFactory       Search Results
     */
    public function __construct(
        SizeResource $resource,
        SizeFactory $sizeFactory,
        SizeCollectionFactory $sizeCollectionFactory,
        SizePivotCollectionFactory $sizePivotCollectionFactory,
        SizeSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->sizeFactory = $sizeFactory;
        $this->sizeCollectionFactory = $sizeCollectionFactory;
        $this->sizePivotCollectionFactory = $sizePivotCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @param Size|SizeInterface $size Size Object
     *
     * @return SizeInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(SizeInterface $size)
    {
        try {
            $this->resource->save($size);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save the size: %1',
                    $exception->getMessage()
                )
            );
        }

        return $size;
    }

    /**
     * Get a Size by ID
     *
     * @param string $sizeId Size ID
     *
     * @return Size|SizeInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById($sizeId)
    {
        /* @var $size Size */
        $size = $this->sizeFactory->create();
        $this->resource->load($size, $sizeId);

        if (!$size->getId()) {
            throw new NoSuchEntityException(
                __('Size with id %1 does not exist.', $sizeId)
            );
        }

        return $size;
    }

    /**
     * {@inheritDoc}
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return SizeSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->sizeCollectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }

        $sortOrders = $searchCriteria->getSortOrders();

        if ($sortOrders) {
            /* @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    (
                        $sortOrder->getDirection() == SortOrder::SORT_ASC
                    ) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * {@inheritDoc}
     *
     * @param Size|SizeInterface $size Size
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException
     */
    public function delete(SizeInterface $size)
    {
        $sizePivotCollection = $this->sizePivotCollectionFactory->create();

        $sizePivotCollection->addFieldToFilter('size_id', $size->getId());

        if ($sizePivotCollection->count() > 0) {
            throw new CouldNotDeleteException(
                __('Could not delete! The size has one or more Size Guide(s) associated.')
            );
        }

        try {
            $this->resource->delete($size);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the Size: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $sizeId Size ID
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException|LocalizedException|NoSuchEntityException
     */
    public function deleteById($sizeId)
    {
        return $this->delete($this->getById($sizeId));
    }

}
