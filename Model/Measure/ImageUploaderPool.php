<?php

namespace Example\SizeGuide\Model\Measure;

use Magento\Framework\ObjectManagerInterface;

/**
 * Class ImageUploaderPool
 *
 * @category Model
 * @package  Example\SizeGuide\Model\Measure
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class ImageUploaderPool
{
    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Image Uploaders
     *
     * @var array|ImageUploader[]
     */
    protected $uploaders;

    /**
     * ImageUploaderPool constructor.
     *
     * @param ObjectManagerInterface $objectManager Object Manager
     * @param array|ImageUploader[]  $uploaders     ImageUploader array
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        array $uploaders = []
    ) {
        $this->objectManager = $objectManager;
        $this->uploaders     = $uploaders;
    }

    /**
     * Get Image Uploader
     *
     * @param string $type Type
     *
     * @return ImageUploader
     *
     * @throws \Exception
     */
    public function getUploader($type)
    {
        if (!isset($this->uploaders[$type])) {
            throw new \Exception(__('ImageUploader not found for type: %1', $type));
        }

        if (!is_object($this->uploaders[$type])) {
            $this->uploaders[$type] = $this->objectManager
                ->create($this->uploaders[$type]);
        }

        $uploader = $this->uploaders[$type];

        if (!($uploader instanceof ImageUploader)) {
            throw new \Exception(
                __(
                    'ImageUploader for type %1 not instance of %2',
                    $type,
                    ImageUploader::class
                )
            );
        }

        return $uploader;
    }
}