<?php

namespace Example\SizeGuide\Model\Measure;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Framework\UrlInterface;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ImageUploader
 *
 * @category Model
 * @package  Example\SizeGuide\Model\Measure
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class ImageUploader
{
    const DEFAULT_BASE_TMP_PATH = 'example_sizeguide/measure/tmp';
    const DEFAULT_BASE_PATH = 'example_sizeguide/measure';

    /**
     * Core file storage database
     *
     * @var Database
     */
    protected $coreFileStorageDatabase;

    /**
     * Media directory object (writable).
     *
     * @var WriteInterface
     */
    protected $mediaDirectory;

    /**
     * Uploader factory
     *
     * @var UploaderFactory
     */
    private $_uploaderFactory;

    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Base tmp path
     *
     * @var string
     */
    protected $baseTmpPath;

    /**
     * Base path
     *
     * @var string
     */
    protected $basePath;

    /**
     * Allowed extensions
     *
     * @var string
     */
    protected $allowedExtensions;

    /**
     * ImageUploader constructor.
     *
     * @param Database              $coreFileStorageDatabase Database
     * @param Filesystem            $filesystem              Filesystem
     * @param UploaderFactory       $uploaderFactory         Uploader
     * @param StoreManagerInterface $storeManager            Store Manager
     * @param LoggerInterface       $logger                  Logger
     * @param null                  $baseTmpPath             Base Temporary Path
     * @param null                  $basePath                Base Path
     * @param null                  $allowedExtensions       Allowed Extensions
     *
     * @throws FileSystemException
     */
    public function __construct(
        Database $coreFileStorageDatabase,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        $baseTmpPath = null,
        $basePath = null,
        $allowedExtensions = null
    ) {
        $this->coreFileStorageDatabase = $coreFileStorageDatabase;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_uploaderFactory = $uploaderFactory;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->baseTmpPath = ($baseTmpPath != null )
            ? $baseTmpPath : self::DEFAULT_BASE_TMP_PATH;
        $this->basePath = ($basePath != null) ? $basePath : self::DEFAULT_BASE_PATH;
        $this->allowedExtensions = ($allowedExtensions != null)
            ? $allowedExtensions : ['jpg', 'jpeg', 'gif', 'png'];
    }

    /**
     * Retrieve base tmp path
     *
     * @return string
     */
    public function getBaseTmpPath()
    {
        return $this->baseTmpPath;
    }

    /**
     * Set base tmp path
     *
     * @param string $baseTmpPath Base Path Temporary
     *
     * @return void
     */
    public function setBaseTmpPath($baseTmpPath)
    {
        $this->baseTmpPath = $baseTmpPath;
    }

    /**
     * Retrieve base path
     *
     * @return string
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * Set base path
     *
     * @param string $basePath Base Path
     *
     * @return void
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * Retrieve base path
     *
     * @return string[]
     */
    public function getAllowedExtensions()
    {
        return $this->allowedExtensions;
    }

    /**
     * Set allowed extensions
     *
     * @param string[] $allowedExtensions Allowed Extensions
     *
     * @return void
     */
    public function setAllowedExtensions($allowedExtensions)
    {
        $this->allowedExtensions = $allowedExtensions;
    }

    /**
     * Retrieve path
     *
     * @param string $path      Image Path
     * @param string $imageName Image name
     *
     * @return string
     */
    public function getFilePath($path, $imageName)
    {
        return rtrim($path, '/') . '/' . ltrim($imageName, '/');
    }

    /**
     * Checking file for save and save it to tmp dir
     *
     * @param string $fileId File ID
     *
     * @return string[]
     *
     * @throws LocalizedException|\Exception
     */
    public function saveFileToTmpDir($fileId)
    {
        $baseTmpPath = $this->getBaseTmpPath();

        $uploader = $this->_uploaderFactory->create(['fileId' => $fileId]);
        $uploader->setAllowedExtensions($this->getAllowedExtensions());
        $uploader->setAllowRenameFiles(false);

        $result = $uploader->save(
            $this->mediaDirectory->getAbsolutePath($baseTmpPath)
        );

        if (!$result) {
            throw new LocalizedException(
                __('File can not be saved to the destination folder.')
            );
        }

        /**
         * Workaround for prototype 1.7 methods "isJSON", "evalJSON" on Windows OS
         */
        $result['tmp_name'] = str_replace('\\', '/', $result['tmp_name']);
        $result['path'] = str_replace('\\', '/', $result['path']);

        $result['url'] = $this->getBaseUrl()
            . $this->getFilePath($baseTmpPath, $result['file']);

        $result['name'] = $result['file'];

        if (isset($result['file'])) {
            try {
                $relativePath = rtrim($baseTmpPath, '/')
                    . '/' . ltrim($result['file'], '/');

                $this->coreFileStorageDatabase->saveFile($relativePath);
            } catch (\Exception $e) {
                $this->logger->critical($e);

                throw new LocalizedException(
                    __('Something went wrong while saving the file(s).')
                );
            }
        }

        return $result;
    }

    /**
     * Checking file for moving and move it
     *
     * @param string $imageName Image name
     *
     * @return string
     *
     * @throws LocalizedException
     */
    public function moveFileFromTmp($imageName)
    {
        $baseTmpPath = $this->getBaseTmpPath();
        $basePath = $this->getBasePath();
        $baseImagePath = $this->getFilePath($basePath, $imageName);
        $baseTmpImagePath = $this->getFilePath($baseTmpPath, $imageName);

        try {
            $this->coreFileStorageDatabase->copyFile(
                $baseTmpImagePath,
                $baseImagePath
            );

            $this->mediaDirectory->renameFile(
                $baseTmpImagePath,
                $baseImagePath
            );

            $this->mediaDirectory->delete($baseTmpImagePath);
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Something went wrong while saving the file(s).')
            );
        }

        return $imageName;
    }

    /**
     * Upload file and get name
     *
     * @param string $input Input name
     * @param array  $data  Input data
     *
     * @return string
     */
    public function uploadFileAndGetName($input, $data)
    {
        if (!isset($data[$input])) {
            return '';
        }

        if (is_array($data[$input]) && !empty($data[$input]['delete'])) {
            return '';
        }

        if (isset($data[$input][0]['name']) && isset($data[$input][0]['tmp_name'])) {
            try {
                $result = $this->moveFileFromTmp($data[$input][0]['file']);
                return $result;
            } catch (\Exception $e) {
                return '';
            }
        } elseif (isset($data[$input][0]['name'])) {
            return $data[$input][0]['name'];
        }

        return '';
    }

    /**
     * Delete Image file
     *
     * @param string $imageName Image file name
     *
     * @return bool
     *
     * @throws FileSystemException
     */
    public function deleteFile($imageName)
    {
        $basePath = $this->getBasePath();
        $baseImagePath = $this->getFilePath($basePath, $imageName);

        return $this->mediaDirectory->delete($baseImagePath);
    }

    /**
     * Get Upload Base URL
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->storeManager
            ->getStore()
            ->getBaseUrl(
                UrlInterface::URL_TYPE_MEDIA
            );
    }
}
