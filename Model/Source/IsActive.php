<?php

namespace Example\SizeGuide\Model\Source;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Model\SizeGuide;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 *
 * @category Source
 * @package  Example\SizeGuide\Model\Source
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class IsActive implements OptionSourceInterface
{
    /**
     * SizeGuide Interface
     *
     * @var SizeGuide|SizeGuideInterface
     */
    protected $sizeGuide;

    /**
     * IsActive constructor.
     *
     * @param SizeGuideInterface $sizeGuideInterface Size Guide Interface
     */
    public function __construct(SizeGuideInterface $sizeGuideInterface)
    {
        $this->sizeGuide = $sizeGuideInterface;
    }

    /**
     * Get option as array
     *
     * @return array
     */
    public function toArray()
    {
        return $this->sizeGuide->getAvailableStatuses();
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->sizeGuide->getAvailableStatuses();

        $options = [];

        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        return $options;
    }

}
