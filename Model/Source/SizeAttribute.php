<?php

namespace Example\SizeGuide\Model\Source;

use Magento\Eav\Model\Config;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class SizeAttribute
 *
 * @category Source
 * @package  Example\SizeGuide\Model\Source
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeAttribute implements OptionSourceInterface
{
    const ATTRIBUTE_CODE = 'size';

    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * SizeAttribute constructor.
     *
     * @param ObjectManagerInterface $objectManager Object Manager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     *
     * @throws LocalizedException
     */
    public function toOptionArray()
    {
        /* @var Config $eavConfig */
        $eavConfig = $this->objectManager->get('\Magento\Eav\Model\Config');

        $attribute = $eavConfig->getAttribute(
            'catalog_product',
            self::ATTRIBUTE_CODE
        );

        $allOptions = $attribute->getSource()->getAllOptions();

        $options = [];

        foreach ($allOptions as $option) {
            $options[] = [
                'label' => $option['label'],
                'value' => $option['value'],
            ];
        }

        return $options;
    }

}
