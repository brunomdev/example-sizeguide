<?php

namespace Example\SizeGuide\Model\Source;

/**
 * Class IsActiveFilter
 *
 * @category Source
 * @package  Example\SizeGuide\Model\Source
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class IsActiveFilter extends IsActive
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array_merge(
            [['label' => '', 'value' => '']], parent::toOptionArray()
        );
    }
}