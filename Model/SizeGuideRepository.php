<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Api\Data\SizeGuideSearchResultsInterface;
use Example\SizeGuide\Api\Data\SizeGuideSearchResultsInterfaceFactory;
use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Example\SizeGuide\Model\Resource\SizeGuide as SizeGuideResource;
use Example\SizeGuide\Model\Resource\SizeGuide\CollectionFactory as SizeGuideCollectionFactory;

/**
 * Class SizeGuideRepository
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeGuideRepository implements SizeGuideRepositoryInterface
{
    /**
     * Size Resource
     *
     * @var SizeGuideResource
     */
    protected $resource;

    /**
     * Size Factory
     *
     * @var SizeGuideFactory
     */
    protected $sizeGuideFactory;

    /**
     * Size Collection Factory
     *
     * @var SizeGuideCollectionFactory
     */
    protected $sizeGuideCollectionFactory;

    /**
     * Size Search Results Factory
     *
     * @var SizeGuideSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * SizeRepository constructor.
     *
     * @param SizeGuideResource                      $resource                   Size Resource
     * @param SizeGuideFactory                       $sizeGuideFactory           Size Factory
     * @param SizeGuideCollectionFactory             $sizeGuideCollectionFactory SizeCollection
     * @param SizeGuideSearchResultsInterfaceFactory $searchResultsFactory       Search Results
     */
    public function __construct(
        SizeGuideResource $resource,
        SizeGuideFactory $sizeGuideFactory,
        SizeGuideCollectionFactory $sizeGuideCollectionFactory,
        SizeGuideSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->sizeGuideFactory = $sizeGuideFactory;
        $this->sizeGuideCollectionFactory = $sizeGuideCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @param SizeGuide|SizeGuideInterface $sizeGuide SizeGuide Object
     *
     * @return SizeGuideInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(SizeGuideInterface $sizeGuide)
    {
        try {
            $this->resource->save($sizeGuide);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save the size: %1',
                    $exception->getMessage()
                )
            );
        }

        return $sizeGuide;
    }

    /**
     * Get a Size by ID
     *
     * @param string $sizeGuideId SizeGuide ID
     *
     * @return SizeGuide|SizeGuideInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById($sizeGuideId)
    {
        /* @var $sizeGuide SizeGuide */
        $sizeGuide = $this->sizeGuideFactory->create();
        $this->resource->load($sizeGuide, $sizeGuideId);

        if (!$sizeGuide->getId()) {
            throw new NoSuchEntityException(
                __('Size Guide with id %1 does not exist.', $sizeGuideId)
            );
        }

        return $sizeGuide;
    }

    /**
     * {@inheritDoc}
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return SizeGuideSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->sizeGuideCollectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }

        $sortOrders = $searchCriteria->getSortOrders();

        if ($sortOrders) {
            /* @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    (
                        $sortOrder->getDirection() == SortOrder::SORT_ASC
                    ) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * {@inheritDoc}
     *
     * @param SizeGuide|SizeGuideInterface $sizeGuide SizeGuide
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException
     */
    public function delete(SizeGuideInterface $sizeGuide)
    {
        try {
            $this->resource->delete($sizeGuide);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the Size: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $sizeGuideId SizeGuide ID
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException|LocalizedException|NoSuchEntityException
     */
    public function deleteById($sizeGuideId)
    {
        return $this->delete($this->getById($sizeGuideId));
    }
}