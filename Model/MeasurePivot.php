<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\MeasurePivotInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class MeasurePivot
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasurePivot extends AbstractModel
    implements MeasurePivotInterface, IdentityInterface
{
    const CACHE_TAG = 'example_sizeguide_measure_pivot';

    /**
     * Cache Tag
     *
     * @var string
     */
    protected $_cacheTag = 'example_sizeguide_measure_pivot';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'example_sizeguide_measure_pivot';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function _construct()
    {
        $this->setIdFieldName(self::ID);

        $this->_init('Example\SizeGuide\Model\Resource\MeasurePivot');
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getSizeGuideId()
    {
        return $this->getData(self::SIZE_GUIDE_ID);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $sizeGuideId SizeGuide ID
     *
     * @return $this
     */
    public function setSizeGuideId($sizeGuideId)
    {
        $this->setData(self::SIZE_GUIDE_ID, $sizeGuideId);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getMeasureId()
    {
        return $this->getData(self::MEASURE_ID);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $measureId Measure ID
     *
     * @return $this
     */
    public function setMeasureId($measureId)
    {
        $this->setData(self::MEASURE_ID, $measureId);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->getData(self::POSITION);
    }
    /**
     * {@inheritdoc}
     *
     * @param int $position Position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->setData(self::POSITION, $position);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
