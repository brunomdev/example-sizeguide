<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\SizePivotInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class SizePivot
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizePivot extends AbstractModel
    implements SizePivotInterface, IdentityInterface
{
    const CACHE_TAG = 'example_sizeguide_size_pivot';

    /**
     * Cache Tag
     *
     * @var string
     */
    protected $_cacheTag = 'example_sizeguide_size_pivot';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'example_sizeguide_size_pivot';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function _construct()
    {
        $this->setIdFieldName(self::ID);

        $this->_init('Example\SizeGuide\Model\Resource\SizePivot');
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getSizeGuideId()
    {
        return $this->getData(self::SIZE_GUIDE_ID);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $sizeGuideId SizeGuide ID
     *
     * @return $this
     */
    public function setSizeGuideId($sizeGuideId)
    {
        $this->setData(self::SIZE_GUIDE_ID, $sizeGuideId);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getSizeId()
    {
        return $this->getData(self::SIZE_ID);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $sizeId Size id
     *
     * @return $this
     */
    public function setSizeId($sizeId)
    {
        $this->setData(self::SIZE_ID, $sizeId);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->getData(self::POSITION);
    }
    /**
     * {@inheritdoc}
     *
     * @param int $position Position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->setData(self::POSITION, $position);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
