<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\MeasureInterface;
use Example\SizeGuide\Model\Measure\ImageUploaderPool;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Class Measure
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Measure extends AbstractModel implements MeasureInterface, IdentityInterface
{
    const CACHE_TAG = 'example_sizeguide_measure';

    /**
     * Cache Tag
     *
     * @var string
     */
    protected $_cacheTag = 'example_sizeguide_measure';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'example_sizeguide_measure';

    /**
     * Image Uplodar Pool
     *
     * @var ImageUploaderPool
     */
    protected $imageUploaderPool;

    /**
     * Measure constructor.
     *
     * @param Context               $context            Context
     * @param Registry              $registry           Registry
     * @param ImageUploaderPool     $imageUploaderPool  Image Uploader Pool
     * @param AbstractResource|null $resource           Resource
     * @param AbstractDb|null       $resourceCollection Resource Collection
     * @param array                 $data               Data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ImageUploaderPool $imageUploaderPool,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->imageUploaderPool = $imageUploaderPool;

        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    public function _construct()
    {
        $this->setIdFieldName(self::ID);

        $this->_init('Example\SizeGuide\Model\Resource\Measure');
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $name Measure name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $description Measure description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getTag()
    {
        return $this->getData(self::TAG);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $tag Measure tag
     *
     * @return $this
     */
    public function setTag($tag)
    {
        $this->setData(self::TAG, $tag);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * {@inheritDoc}
     *
     * @return bool|string
     *
     * @throws LocalizedException|\Exception
     */
    public function getImageUrl()
    {
        $url = false;
        $image = $this->getImage();
        if ($image) {
            if (is_string($image)) {
                $uploader = $this->imageUploaderPool->getUploader('image');
                $url = $uploader->getBaseUrl()
                    . $uploader->getBasePath() . '/' . $image;
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }

        return $url;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $image Measure image
     *
     * @return $this
     */
    public function setImage($image)
    {
        $this->setData(self::IMAGE, $image);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $createdAt Measure createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(self::CREATED_AT, $createdAt);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $updatedAt Measure updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->setData(self::UPDATED_AT, $updatedAt);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
