<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\MeasureInterface;
use Example\SizeGuide\Api\Data\MeasureSearchResultsInterface;
use Example\SizeGuide\Api\MeasureRepositoryInterface;
use Example\SizeGuide\Api\Data\MeasureSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Example\SizeGuide\Model\Resource\Measure as MeasureResource;
use Example\SizeGuide\Model\Resource\Measure\CollectionFactory as MeasureCollectionFactory;
use Example\SizeGuide\Model\Resource\MeasurePivot\CollectionFactory as MeasurePivotCollectionFactory;

/**
 * Class MeasureRepository
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasureRepository implements MeasureRepositoryInterface
{
    /**
     * Measure Resource
     *
     * @var MeasureResource
     */
    protected $resource;

    /**
     * Measure Factory
     *
     * @var MeasureFactory
     */
    protected $measureFactory;

    /**
     * Measure Collection Factory
     *
     * @var MeasureCollectionFactory
     */
    protected $measureCollectionFactory;

    /**
     * Measure Pivot Collection Factory
     *
     * @var MeasurePivotCollectionFactory
     */
    protected $measurePivotCollectionFactory;

    /**
     * Measure Search Results Factory
     *
     * @var MeasureSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * MeasureRepository constructor.
     *
     * @param MeasureResource                      $resource                      Resource
     * @param MeasureFactory                       $measureFactory                Factory
     * @param MeasureCollectionFactory             $measureCollectionFactory      Collection
     * @param MeasurePivotCollectionFactory        $measurePivotCollectionFactory Collection
     * @param MeasureSearchResultsInterfaceFactory $searchResultsFactory          Results
     */
    public function __construct(
        MeasureResource $resource,
        MeasureFactory $measureFactory,
        MeasureCollectionFactory $measureCollectionFactory,
        MeasurePivotCollectionFactory $measurePivotCollectionFactory,
        MeasureSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->measureFactory = $measureFactory;
        $this->measureCollectionFactory = $measureCollectionFactory;
        $this->measurePivotCollectionFactory = $measurePivotCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @param Measure|MeasureInterface $measure Measure Object
     *
     * @return MeasureInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(MeasureInterface $measure)
    {
        try {
            $this->resource->save($measure);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save the measure: %1',
                    $exception->getMessage()
                )
            );
        }

        return $measure;
    }

    /**
     * Get a Measure by ID
     *
     * @param string $measureId Measure ID
     *
     * @return Measure|MeasureInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById($measureId)
    {
        /* @var $measure Measure */
        $measure = $this->measureFactory->create();
        $this->resource->load($measure, $measureId);

        if (!$measure->getId()) {
            throw new NoSuchEntityException(
                __('Measure with id %1 does not exist.', $measureId)
            );
        }

        return $measure;
    }

    /**
     * {@inheritDoc}
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return MeasureSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->measureCollectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }

        $sortOrders = $searchCriteria->getSortOrders();

        if ($sortOrders) {
            /* @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    (
                        $sortOrder->getDirection() == SortOrder::SORT_ASC
                    ) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * {@inheritDoc}
     *
     * @param Measure|MeasureInterface $measure Measure
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException
     */
    public function delete(MeasureInterface $measure)
    {
        $measurePivotCollection = $this->measurePivotCollectionFactory->create();

        $measurePivotCollection->addFieldToFilter('measure_id', $measure->getId());

        if ($measurePivotCollection->count() > 0) {
            throw new CouldNotDeleteException(
                __('Could not delete! The measure has one or more Size Guide(s) associated.')
            );
        }

        try {
            $this->resource->delete($measure);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the Measure: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $measureId Measure ID
     *
     * @return bool|true
     *
     * @throws CouldNotDeleteException|LocalizedException|NoSuchEntityException
     */
    public function deleteById($measureId)
    {
        return $this->delete($this->getById($measureId));
    }

}
