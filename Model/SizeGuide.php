<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class SizeGuide
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeGuide extends AbstractModel
    implements SizeGuideInterface, IdentityInterface
{
    /**
     * Cache Tag
     */
    const CACHE_TAG = 'example_sizeguide';

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Cache Tag
     *
     * @var string
     */
    protected $_cacheTag = 'example_sizeguide';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'example_sizeguide';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function _construct()
    {
        $this->setIdFieldName(self::ID);

        $this->_init('Example\SizeGuide\Model\Resource\SizeGuide');
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $name SizeGuide name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * {@inheritdoc}
     *
     * @param bool $isActive SizeGuide is active
     *
     * @return $this
     */
    public function setIsActive($isActive)
    {
        $this->getData(self::IS_ACTIVE, $isActive);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $description SizeGuide description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $createdAt SizeGuide createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(self::CREATED_AT, $createdAt);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $updatedAt SizeGuide updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->setData(self::UPDATED_AT, $updatedAt);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Prepare SizeGuide statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
            self::STATUS_ENABLED => __('Yes'),
            self::STATUS_DISABLED => __('No')
        ];
    }

}
