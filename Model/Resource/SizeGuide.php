<?php

namespace Example\SizeGuide\Model\Resource;

use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class SizeGuide
 *
 * @category Resource
 * @package  Example\SizeGuide\Model\Resource
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeGuide extends AbstractDb
{
    /**
     * ID field name
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Entity Manager
     *
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('example_sizeguide', 'id');
    }

    /**
     * Perform actions after object save
     *
     * @param AbstractModel $object Abstract Model
     *
     * @return AbstractDb
     */
    protected function _afterSave(AbstractModel $object)
    {
        // Assign Sizes to item
        if ($object->hasData('size_ids')) {
            $where = ['sizeguide_id = ?' => (int)$object->getId()];
            $this->getConnection()
                ->delete(
                    $this->getTable('example_sizeguide_size_pivot'), $where
                );

            $sizeData = [];
            $sizeIds = (array)$object->getData('size_ids');
            if (count($sizeIds) > 0) {
                foreach ($sizeIds as $sizeId => $pos) {
                    $position = $pos['position'];
                    if ($position == '') {
                        $position = 0;
                    }

                    $sizeData[] = [
                        'sizeguide_id' => (int)$object->getId(),
                        'size_id' => $sizeId,
                        'position' => $position
                    ];
                }

                $this->getConnection()
                    ->insertMultiple(
                        $this->getTable('example_sizeguide_size_pivot'),
                        $sizeData
                    );
            }
        }

        // Assign Measures to item
        if ($object->hasData('measure_ids')) {
            $where = ['sizeguide_id = ?' => (int)$object->getId()];
            $this->getConnection()
                ->delete(
                    $this->getTable('example_sizeguide_measure_pivot'), $where
                );

            $measureData = [];
            $measureIds = (array)$object->getData('measure_ids');
            if (count($measureIds) > 0) {
                foreach ($measureIds as $measureId => $pos) {
                    $position = $pos['position'];
                    if ($position == '') {
                        $position = 0;
                    }

                    $measureData[] = [
                        'sizeguide_id' => (int)$object->getId(),
                        'measure_id' => $measureId,
                        'position' => $position
                    ];
                }

                $this->getConnection()
                    ->insertMultiple(
                        $this->getTable('example_sizeguide_measure_pivot'),
                        $measureData
                    );
            }
        }

        return parent::_afterSave($object);
    }

    /**
     * Perform actions before object delete
     *
     * @param AbstractModel $object Abstract Model
     *
     * @return AbstractDb
     */
    protected function _beforeDelete(AbstractModel $object)
    {
        $condition = ['sizeguide_id = ?' => (int)$object->getId()];

        // Detach Sizes from item
        $this->getConnection()->delete(
            $this->getTable('example_sizeguide_size_pivot'),
            $condition
        );

        // Detach Measures from item
        $this->getConnection()->delete(
            $this->getTable('example_sizeguide_measure_pivot'),
            $condition
        );

        return parent::_beforeDelete($object);
    }
}
