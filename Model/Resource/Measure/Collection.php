<?php

namespace Example\SizeGuide\Model\Resource\Measure;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @category Collection
 * @package  Example\SizeGuide\Model\Resource\Measure
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Collection extends AbstractCollection
{
    /**
     * ID field name
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Example\SizeGuide\Model\Measure',
            'Example\SizeGuide\Model\Resource\Measure'
        );
    }
}
