<?php

namespace Example\SizeGuide\Model\Resource;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class SizePivot
 *
 * @category Resource
 * @package  Example\SizeGuide\Model\Resource
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizePivot extends AbstractDb
{
    /**
     * ID field name
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Entity Manager
     *
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('example_sizeguide_size_pivot', 'id');
    }

    /**
     * Save entity's attributes into the object's resource
     *
     * @param AbstractModel $object Size Model
     *
     * @return $this|AbstractDb
     *
     * @throws \Exception
     */
    public function save(AbstractModel $object)
    {
        $this->_getEntityManager()->save($object);

        return $this;
    }

    /**
     * Get Entity Manager
     *
     * @return EntityManager
     */
    private function _getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->entityManager = ObjectManager::getInstance()
                ->get('Magento\Framework\EntityManager\EntityManager');
        }

        return $this->entityManager;
    }

}
