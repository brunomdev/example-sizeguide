<?php

namespace Example\SizeGuide\Model;

use Example\SizeGuide\Api\Data\SizeInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Size
 *
 * @category Model
 * @package  Example\SizeGuide\Model
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Size extends AbstractModel implements SizeInterface, IdentityInterface
{
    const CACHE_TAG = 'example_sizeguide_size';

    /**
     * Cache Tag
     *
     * @var string
     */
    protected $_cacheTag = 'example_sizeguide_size';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'example_sizeguide_size';

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    public function _construct()
    {
        $this->setIdFieldName(self::ID);

        $this->_init('Example\SizeGuide\Model\Resource\Size');
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getCode()
    {
        return $this->getData(self::CODE);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $code Size code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->setData(self::CODE, $code);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $name Size name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $description Size description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $createdAt Size createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(self::CREATED_AT, $createdAt);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * {@inheritDoc}
     *
     * @param string $updatedAt Size updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->setData(self::UPDATED_AT, $updatedAt);

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
