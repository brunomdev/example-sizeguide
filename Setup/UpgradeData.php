<?php

namespace Example\SizeGuide\Setup;

use Example\SizeGuide\Model\Entity\Attribute\Backend\SizeGuide;
use Magento\Catalog\Model\Product\Type;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Downloadable\Model\Product\Type as DownloadableType;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 *
 * @category Setup
 * @package  Example\SizeGuide\Setup
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Eav Setup Factory
     *
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;

    /**
     * InstallData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory Eav Setup
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     *
     * @param ModuleDataSetupInterface $setup   Setup
     * @param ModuleContextInterface   $context Context
     *
     * @return void
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        if (version_compare($context->getVersion(), '1.0.5', '<=')) {
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);

            $productTypes = [
                Type::TYPE_SIMPLE,
                Configurable::TYPE_CODE,
                Type::TYPE_VIRTUAL,
                DownloadableType::TYPE_DOWNLOADABLE,
                Type::TYPE_BUNDLE,
            ];

            $productTypes = join(',', $productTypes);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'sizeguide_id',
                [
                    'group' => 'Product Details',
                    'sort_order' => 200,
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => __('Size Guide'),
                    'input' => 'select',
                    'class' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'source' => SizeGuide::class,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_in_advanced_search' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => $productTypes,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                ]
            );
        }
    }
}
