<?php

namespace Example\SizeGuide\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @category Setup
 * @package  Example\SizeGuide\Setup
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @param SchemaSetupInterface   $setup   SchemaSetup
     * @param ModuleContextInterface $context ModuleContext
     *
     * @return void
     *
     * @throws \Zend_Db_Exception
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $tableSizeGuide = $setup->getTable('example_sizeguide');
        if (!$setup->getConnection()->isTableExists($tableSizeGuide)) {
            $table = $setup->getConnection()
                ->newTable($tableSizeGuide)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Size Guide ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    150,
                    ['nullable' => false],
                    'Name'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Description'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => false],
                    'Is Active'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );

            $setup->getConnection()->createTable($table);
        }

        $tableSizes = $setup->getTable('example_sizeguide_size');
        if (!$setup->getConnection()->isTableExists($tableSizes)) {
            $table = $setup->getConnection()
                ->newTable($tableSizes)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Size Guide Sizes ID'
                )
                ->addColumn(
                    'code',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => false, 'default' => false],
                    'Code'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    150,
                    ['nullable' => false],
                    'Name'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Description'
                )
                ->addColumn(
                    'order',
                    Table::TYPE_INTEGER,
                    3,
                    ['nullable' => false],
                    'Order'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );

            $setup->getConnection()->createTable($table);
        }

        $tableSizesPivot = $setup->getTable('example_sizeguide_size_pivot');
        if (!$setup->getConnection()->isTableExists($tableSizesPivot)) {
            $table = $setup->getConnection()
                ->newTable($tableSizesPivot)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Size Guide Sizes ID'
                )
                ->addColumn(
                    'sizeguide_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Size Guide ID FK'
                )
                ->addColumn(
                    'size_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Size Guide Size ID FK'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'example_sizeguide_size_pivot',
                        'sizeguide_id',
                        'example_sizeguide',
                        'id'
                    ),
                    'sizeguide_id',
                    $tableSizeGuide,
                    'id'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'example_sizeguide_size_pivot',
                        'size_id',
                        'example_sizeguide_size',
                        'id'
                    ),
                    'size_id',
                    $tableSizes,
                    'id'
                );

            $setup->getConnection()->createTable($table);
        }

        $tableMeasures = $setup->getTable('example_sizeguide_measure');
        if (!$setup->getConnection()->isTableExists($tableMeasures)) {
            $table = $setup->getConnection()
                ->newTable($tableMeasures)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Size Guide Measures ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    150,
                    ['nullable' => false],
                    'Name'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Description'
                )
                ->addColumn(
                    'order',
                    Table::TYPE_INTEGER,
                    3,
                    ['nullable' => false],
                    'Description'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );

            $setup->getConnection()->createTable($table);
        }

        $tableMeasuresPivot = $setup
            ->getTable('example_sizeguide_measure_pivot');
        if (!$setup->getConnection()->isTableExists($tableMeasuresPivot)) {
            $table = $setup->getConnection()
                ->newTable($tableMeasuresPivot)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Size Guide Sizes ID'
                )
                ->addColumn(
                    'sizeguide_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Size Guide ID FK'
                )
                ->addColumn(
                    'measure_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Size Guide Measure ID FK'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'example_sizeguide_measure_pivot',
                        'sizeguide_id',
                        'example_sizeguide',
                        'id'
                    ),
                    'sizeguide_id',
                    $tableSizeGuide,
                    'id'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'example_sizeguide_measure_pivot',
                        'measure_id',
                        'example_sizeguide_measure',
                        'id'
                    ),
                    'measure_id',
                    $tableMeasures,
                    'id'
                );

            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();
    }

}
