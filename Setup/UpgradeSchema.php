<?php

namespace Example\SizeGuide\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 *
 * @category Setup
 * @package  Example\SizeGuide\Setup
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @param SchemaSetupInterface   $setup   Setup
     * @param ModuleContextInterface $context Context
     *
     * @return void
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $tableSizeGuide = $setup->getTable('example_sizeguide');
        $tableMeasure = $setup->getTable('example_sizeguide_measure');
        $tableMeasurePivot = $setup->getTable('example_sizeguide_measure_pivot');
        $tableSize = $setup->getTable('example_sizeguide_size');
        $tableSizePivot = $setup->getTable('example_sizeguide_size_pivot');

        if (version_compare($context->getVersion(), '1.0.1', '<=')) {
            $setup->getConnection()->addColumn(
                $tableMeasure,
                'image',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'after' => 'description',
                    'comment' => 'Measure Image'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.2', '<=')) {
            $setup->getConnection()->dropColumn($tableSize, 'code');
            $setup->getConnection()->dropColumn($tableSize, 'description');
        }

        if (version_compare($context->getVersion(), '1.0.3', '<=')) {
            if ($setup->getConnection()->tableColumnExists($tableMeasure, 'order')) {
                $setup->getConnection()->dropColumn($tableMeasure, 'order');
            }

            if ($setup->getConnection()->tableColumnExists($tableSize, 'order')) {
                $setup->getConnection()->dropColumn($tableSize, 'order');
            }

            $setup->getConnection()->addColumn(
                $tableMeasurePivot,
                'position',
                [
                    'type' => Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'after' => 'measure_id',
                    'comment' => 'Measure Position'
                ]
            );

            $setup->getConnection()->addColumn(
                $tableSizePivot,
                'position',
                [
                    'type' => Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'after' => 'size_id',
                    'comment' => 'Size Position'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.4', '<=')) {
            $setup->getConnection()->addColumn(
                $tableSizeGuide,
                'size_table',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'after' => 'is_active',
                    'comment' => 'Size Guide Size Table'
                ]
            );

            $setup->getConnection()->addColumn(
                $tableSizeGuide,
                'measure_table',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'after' => 'size_table',
                    'comment' => 'Size Guide Measure Table'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.5', '<=')) {
            $setup->getConnection()->addColumn(
                $tableMeasure,
                'tag',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => '255',
                    'nullable' => true,
                    'after' => 'description',
                    'comment' => 'Measure Tag'
                ]
            );
        }

        $setup->endSetup();
    }

}
