<?php

namespace Example\SizeGuide\Api;

use Example\SizeGuide\Api\Data\SizePivotInterface;
use Example\SizeGuide\Api\Data\SizePivotSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface SizePivotRepositoryInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizePivotRepositoryInterface
{
    /**
     * Save Size
     *
     * @param SizePivotInterface $size Size Pivot
     *
     * @return SizePivotInterface
     *
     * @throws LocalizedException
     */
    public function save(SizePivotInterface $size);

    /**
     * Retrieve Size
     *
     * @param string $sizeId Size Pivot ID
     *
     * @return SizePivotInterface
     *
     * @throws LocalizedException
     */
    public function getById($sizeId);

    /**
     * Retrieve Size matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return SizePivotSearchResultsInterface
     *
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Size
     *
     * @param SizePivotInterface $size Size Pivot
     *
     * @return bool|true
     *
     * @throws LocalizedException
     */
    public function delete(SizePivotInterface $size);

    /**
     * Delete Size by ID
     *
     * @param string $sizeId Size Pivot ID
     *
     * @return bool|true
     *
     * @throws NoSuchEntityException|LocalizedException
     */
    public function deleteById($sizeId);
}
