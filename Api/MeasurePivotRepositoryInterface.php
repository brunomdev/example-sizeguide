<?php

namespace Example\SizeGuide\Api;

use Example\SizeGuide\Api\Data\MeasurePivotInterface;
use Example\SizeGuide\Api\Data\MeasurePivotSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface MeasurePivotRepositoryInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface MeasurePivotRepositoryInterface
{
    /**
     * Save Size
     *
     * @param MeasurePivotInterface $measurePivot Measure Pivot
     *
     * @return MeasurePivotInterface
     *
     * @throws LocalizedException
     */
    public function save(MeasurePivotInterface $measurePivot);

    /**
     * Retrieve Size
     *
     * @param string $measurePivotId Measure Pivot ID
     *
     * @return MeasurePivotInterface
     *
     * @throws LocalizedException
     */
    public function getById($measurePivotId);

    /**
     * Retrieve Size matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return MeasurePivotSearchResultsInterface
     *
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Size
     *
     * @param MeasurePivotInterface $measurePivot Measure Pivot
     *
     * @return bool|true
     *
     * @throws LocalizedException
     */
    public function delete(MeasurePivotInterface $measurePivot);

    /**
     * Delete Size by ID
     *
     * @param string $measurePivotId Measure Pivot ID
     *
     * @return bool|true
     *
     * @throws NoSuchEntityException|LocalizedException
     */
    public function deleteById($measurePivotId);
}
