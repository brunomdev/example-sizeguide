<?php

namespace Example\SizeGuide\Api;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Api\Data\SizeGuideSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface SizeGuideRepositoryInterface
 *
 * @category Data
 * @package  Example\SizeGuideGuide\Api
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizeGuideRepositoryInterface
{
    /**
     * Save SizeGuide
     *
     * @param SizeGuideInterface $sizeGuide SizeGuide
     *
     * @return SizeGuideInterface
     *
     * @throws LocalizedException
     */
    public function save(SizeGuideInterface $sizeGuide);

    /**
     * Retrieve SizeGuide
     *
     * @param string $sizeGuideId SizeGuide ID
     *
     * @return SizeGuideInterface
     *
     * @throws LocalizedException
     */
    public function getById($sizeGuideId);

    /**
     * Retrieve SizeGuide matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return SizeGuideSearchResultsInterface
     *
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete SizeGuide
     *
     * @param SizeGuideInterface $sizeGuide SizeGuide
     *
     * @return bool|true
     *
     * @throws LocalizedException
     */
    public function delete(SizeGuideInterface $sizeGuide);

    /**
     * Delete SizeGuide by ID
     *
     * @param string $sizeGuideId SizeGuide ID
     *
     * @return bool|true
     *
     * @throws NoSuchEntityException|LocalizedException
     */
    public function deleteById($sizeGuideId);
}
