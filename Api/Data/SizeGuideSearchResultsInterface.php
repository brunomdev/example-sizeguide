<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface SizeGuideSearchResultsInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizeGuideSearchResultsInterface
{
    /**
     * Get Size list.
     *
     * @return SizeGuideInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     *
     * @param SizeGuideInterface[] $items Size Guide Items
     *
     * @return $this
     */
    public function setItems(array $items);
}