<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface SizeGuideInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizeGuideInterface
{
    const ID = 'id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const IS_ACTIVE = 'is_active';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get SizeGuide id
     *
     * @return int
     */
    public function getId();

    /**
     * Set SizeGuide id
     *
     * @param int $id SizeGuide id
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Get Size name
     *
     * @return string
     */
    public function getName();

    /**
     * Set Size name
     *
     * @param string $name Size name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * Get SizeGuide Is Active
     *
     * @return bool
     */
    public function getIsActive();

    /**
     * Set SizeGuide Is Active
     *
     * @param bool $isActive SizeGuide Is Active
     *
     * @return $this
     */
    public function setIsActive($isActive);

    /**
     * Get SizeGuide description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set SizeGuide description
     *
     * @param string $description SizeGuide description
     *
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get SizeGuide createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set SizeGuide id
     *
     * @param string $createdAt SizeGuide createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get SizeGuide updatedAt
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set SizeGuide updatedAt
     *
     * @param string $updatedAt SizeGuide updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
