<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface MeasurePivotSearchResultsInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface MeasurePivotSearchResultsInterface
{
    /**
     * Get Measure list.
     *
     * @return MeasurePivotInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     *
     * @param MeasurePivotInterface[] $items Measure Pivot Items
     *
     * @return $this
     */
    public function setItems(array $items);
}
