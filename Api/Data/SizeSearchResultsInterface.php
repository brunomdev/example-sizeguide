<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface SizeSearchResultsInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizeSearchResultsInterface
{
    /**
     * Get Size list.
     *
     * @return SizeInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     *
     * @param SizeInterface[] $items Size Items
     *
     * @return $this
     */
    public function setItems(array $items);
}
