<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface MeasureSearchResultsInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface MeasureSearchResultsInterface
{
    /**
     * Get Measure list.
     *
     * @return MeasureInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     *
     * @param MeasureInterface[] $items Measure Items
     *
     * @return $this
     */
    public function setItems(array $items);
}
