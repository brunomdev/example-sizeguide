<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface MeasurePivotInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface MeasurePivotInterface
{
    const ID = 'id';
    const SIZE_GUIDE_ID = 'sizeguide_id';
    const MEASURE_ID = 'measure_id';
    const POSITION = 'position';

    /**
     * Get Measure Pivot ID
     *
     * @return int
     */
    public function getId();

    /**
     * Set Measure Pivot ID
     *
     * @param int $id ID
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Get Measure Pivot SizeGuide ID
     *
     * @return int
     */
    public function getSizeGuideId();

    /**
     * Set Measure Pivot ID
     *
     * @param int $sizeGuideId SizeGuide ID
     *
     * @return $this
     */
    public function setSizeGuideId($sizeGuideId);

    /**
     * Get Measure Pivot Measure ID
     *
     * @return int
     */
    public function getMeasureId();

    /**
     * Set Measure Pivot Measure ID
     *
     * @param int $measureId Measure ID
     *
     * @return $this
     */
    public function setMeasureId($measureId);

    /**
     * Get Measure Pivot position
     *
     * @return int
     */
    public function getPosition();

    /**
     * Set Measure Pivot Position
     *
     * @param int $position Position
     *
     * @return $this
     */
    public function setPosition($position);
}
