<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface MeasureInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface MeasureInterface
{
    const ID = 'id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const TAG = 'tag';
    const IMAGE = 'image';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get Measure id
     *
     * @return int
     */
    public function getId();

    /**
     * Set Measure id
     *
     * @param int $id Measure id
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Get Measure name
     *
     * @return string
     */
    public function getName();

    /**
     * Set Measure name
     *
     * @param string $name Measure name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * Get Measure description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set Measure description
     *
     * @param string $description Measure description
     *
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get Measure tag
     *
     * @return string
     */
    public function getTag();

    /**
     * Set Measure tag
     *
     * @param string $tag Measure tag
     *
     * @return $this
     */
    public function setTag($tag);

    /**
     * Get Measure Image
     *
     * @return string
     */
    public function getImage();

    /**
     * Get Measure Image URL
     *
     * @return bool|string
     */
    public function getImageUrl();

    /**
     * Set Measure image
     *
     * @param string $image Measure image
     *
     * @return $this
     */
    public function setImage($image);

    /**
     * Get Measure createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set Measure id
     *
     * @param string $createdAt Measure createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get Measure updatedAt
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set Measure updatedAt
     *
     * @param string $updatedAt Measure updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
