<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface SizePivotSearchResultsInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizePivotSearchResultsInterface
{
    /**
     * Get Size list.
     *
     * @return SizePivotInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     *
     * @param SizePivotInterface[] $items Size Pivot Items
     *
     * @return $this
     */
    public function setItems(array $items);
}
