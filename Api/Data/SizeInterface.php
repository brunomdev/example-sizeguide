<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface SizeInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizeInterface
{
    const ID = 'id';
    const CODE = 'code';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get Size id
     *
     * @return int
     */
    public function getId();

    /**
     * Set Size id
     *
     * @param int $id Size id
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Get Size code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set Size code
     *
     * @param string $code Size code
     *
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Size name
     *
     * @return string
     */
    public function getName();

    /**
     * Set Size name
     *
     * @param string $name Size name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * Get Size description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set Size description
     *
     * @param string $description Size description
     *
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get Size createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set Size id
     *
     * @param string $createdAt Size createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get Size updatedAt
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set Size updatedAt
     *
     * @param string $updatedAt Size updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
