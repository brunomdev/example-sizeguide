<?php

namespace Example\SizeGuide\Api\Data;

/**
 * Interface SizePivotInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api\Data
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface SizePivotInterface
{
    const ID = 'id';
    const SIZE_GUIDE_ID = 'sizeguide_id';
    const SIZE_ID = 'size_id';
    const POSITION = 'position';

    /**
     * Get Size Pivot id
     *
     * @return int
     */
    public function getId();

    /**
     * Set Size Pivot id
     *
     * @param int $id Measure id
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Get Size Pivot SizeGuide id
     *
     * @return int
     */
    public function getSizeGuideId();

    /**
     * Set Size Pivot id
     *
     * @param int $sizeGuideId SizeGuide id
     *
     * @return $this
     */
    public function setSizeGuideId($sizeGuideId);

    /**
     * Get Size Pivot Size id
     *
     * @return int
     */
    public function getSizeId();

    /**
     * Set Size Pivot Size id
     *
     * @param int $sizeId Size id
     *
     * @return $this
     */
    public function setSizeId($sizeId);

    /**
     * Get Size Pivot position
     *
     * @return int
     */
    public function getPosition();

    /**
     * Set Size Pivot Position
     *
     * @param int $position Position
     *
     * @return $this
     */
    public function setPosition($position);
}
