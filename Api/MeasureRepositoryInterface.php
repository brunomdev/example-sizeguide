<?php

namespace Example\SizeGuide\Api;

use Example\SizeGuide\Api\Data\MeasureInterface;
use Example\SizeGuide\Api\Data\MeasureSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface MeasureRepositoryInterface
 *
 * @category Data
 * @package  Example\SizeGuide\Api
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
interface MeasureRepositoryInterface
{
    /**
     * Save Size
     *
     * @param MeasureInterface $measure Size
     *
     * @return MeasureInterface
     *
     * @throws LocalizedException
     */
    public function save(MeasureInterface $measure);

    /**
     * Retrieve Size
     *
     * @param string $measureId Measure ID
     *
     * @return MeasureInterface
     *
     * @throws LocalizedException
     */
    public function getById($measureId);

    /**
     * Retrieve Size matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria Search Criteria
     *
     * @return MeasureSearchResultsInterface
     *
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Size
     *
     * @param MeasureInterface $measure Measure
     *
     * @return bool|true
     *
     * @throws LocalizedException
     */
    public function delete(MeasureInterface $measure);

    /**
     * Delete Size by ID
     *
     * @param string $measureId Measure ID
     *
     * @return bool|true
     *
     * @throws NoSuchEntityException|LocalizedException
     */
    public function deleteById($measureId);
}
