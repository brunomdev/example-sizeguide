<?php

namespace Example\SizeGuide\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Example\SizeGuide\Block\Adminhtml\Measure\Grid\Renderer\Action\UrlBuilder;
use Magento\Framework\UrlInterface;

/**
 * Class MeasureActions
 *
 * @category Component
 * @package  Example\SizeGuide\Ui\Component\Listing\Columns
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasureActions extends Column
{
    const MEASURE_URL_PATH_EDIT = 'sizeguide/measure/edit';
    const MEASURE_URL_PATH_DELETE = 'sizeguide/measure/delete';

    /**
     * Url Builder
     *
     * @var UrlBuilder
     */
    protected $actionUrlBuilder;

    /**
     * Url Builder Interface
     *
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Edit URL
     *
     * @var string
     */
    private $_editUrl;

    /**
     * MeasureActions constructor.
     *
     * @param ContextInterface   $context            Context
     * @param UiComponentFactory $uiComponentFactory Component Factory
     * @param UrlBuilder         $actionUrlBuilder   Url Builder
     * @param UrlInterface       $urlBuilder         Url Interface
     * @param array              $components         Components
     * @param array              $data               Data
     * @param string             $editUrl            Edit Url
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlBuilder $actionUrlBuilder,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::MEASURE_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->actionUrlBuilder = $actionUrlBuilder;
        $this->_editUrl = $editUrl;

        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepares the Data Source
     *
     * @param array $dataSource Data Source
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['id'])) {
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder
                            ->getUrl($this->_editUrl, ['id' => $item['id']]),
                        'label' => __('Edit')
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder
                            ->getUrl(
                                self::MEASURE_URL_PATH_DELETE,
                                ['id' => $item['id']]
                            ),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete Measure'),
                            'message' => __(
                                'Are you sure you wan\'t to delete this Measure?'
                            )
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }

}
