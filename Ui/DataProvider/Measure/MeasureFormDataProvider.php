<?php

namespace Example\SizeGuide\Ui\DataProvider\Measure;

use Example\SizeGuide\Model\Resource\Measure\Collection;
use Example\SizeGuide\Model\Measure;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Example\SizeGuide\Model\Resource\Measure\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class MeasureFormDataProvider
 *
 * @category DataProvider
 * @package  Example\SizeGuide\Ui\DataProvider\Measure
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasureFormDataProvider extends AbstractDataProvider
{
    /**
     * Measure Collection
     *
     * @var Collection
     */
    protected $collection;

    /**
     * Data Persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Loaded Data
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string                 $name              Name
     * @param string                 $primaryFieldName  Primary Field Name
     * @param string                 $requestFieldName  Request Field Name
     * @param CollectionFactory      $collectionFactory Collection
     * @param DataPersistorInterface $dataPersistor     Data Persistor
     * @param array                  $meta              Meta
     * @param array                  $data              Data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    /**
     * Get data
     *
     * @return array
     *
     * @throws LocalizedException
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /* @var Measure $measureModel */
        foreach ($items as $measureModel) {
            if ($measureModel->hasData('image')) {
                $img = [
                    [
                        'name' => $measureModel->getImage(),
                        'url' => $measureModel->getImageUrl()
                    ]
                ];

                $measureModel->setData('image', $img);
            }

            $this->loadedData[$measureModel->getId()] = $measureModel->getData();
        }

        $data = $this->dataPersistor->get('example_sizeguide_measure');

        if (!empty($data)) {
            $measureModel = $this->collection->getNewEmptyItem();
            $measureModel->setData($data);
            $this->loadedData[$measureModel->getId()] = $measureModel->getData();
            $this->dataPersistor->clear('example_sizeguide_measure');
        }

        return $this->loadedData;
    }
}