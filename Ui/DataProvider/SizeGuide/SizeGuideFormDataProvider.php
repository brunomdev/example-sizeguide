<?php

namespace Example\SizeGuide\Ui\DataProvider\SizeGuide;

use Example\SizeGuide\Model\Resource\SizeGuide\Collection;
use Example\SizeGuide\Model\SizeGuide;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Example\SizeGuide\Model\Resource\SizeGuide\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class SizeGuideFormDataProvider
 *
 * @category DataProvider
 * @package  Example\SizeGuide\Ui\DataProvider\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeGuideFormDataProvider extends AbstractDataProvider
{
    /**
     * Size Collection
     *
     * @var Collection
     */
    protected $collection;

    /**
     * Data Persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Loaded Data
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string                 $name              Name
     * @param string                 $primaryFieldName  Primary Field Name
     * @param string                 $requestFieldName  Request Field Name
     * @param CollectionFactory      $collectionFactory Collection
     * @param DataPersistorInterface $dataPersistor     Data Persistor
     * @param array                  $meta              Meta
     * @param array                  $data              Data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /* @var SizeGUide $sizeGuideModel */
        foreach ($items as $sizeGuideModel) {
            $this->loadedData[$sizeGuideModel->getId()] = $sizeGuideModel->getData();
        }

        $data = $this->dataPersistor->get('example_sizeguide_size');

        if (!empty($data)) {
            $sizeGuideModel = $this->collection->getNewEmptyItem();
            $sizeGuideModel->setData($data);
            $this->loadedData[$sizeGuideModel->getId()] = $sizeGuideModel->getData();
            $this->dataPersistor->clear('example_sizeguide_size');
        }

        return $this->loadedData;
    }
}