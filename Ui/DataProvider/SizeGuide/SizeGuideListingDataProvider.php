<?php

namespace Example\SizeGuide\Ui\DataProvider\SizeGuide;

use Example\SizeGuide\Model\Resource\SizeGuide\Collection;
use Magento\Framework\Api\Filter;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Example\SizeGuide\Model\Resource\SizeGuide\CollectionFactory;

/**
 * Class SizeGuideListingDataProvider
 *
 * @category DataProvider
 * @package  Example\SizeGuide\Ui\DataProvider\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeGuideListingDataProvider extends AbstractDataProvider
{
    /**
     * Size Collection
     *
     * @var Collection
     */
    protected $collection;

    /**
     * Field Strategies
     *
     * @var array
     */
    protected $addFieldStrategies;

    /**
     * Filter Strategies
     *
     * @var array
     */
    protected $addFilterStrategies;

    /**
     * SizeListingDataProvider constructor.
     *
     * @param string            $name                Name
     * @param string            $primaryFieldName    Primary Field Name
     * @param string            $requestFieldName    Request Field Name
     * @param CollectionFactory $collectionFactory   Collection Factory
     * @param array             $addFieldStrategies  Field Strategies
     * @param array             $addFilterStrategies Filter Strategies
     * @param array             $meta                Meta Data
     * @param array             $data                Data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->collection = $collectionFactory->create();
        $this->addFieldStrategies = $addFieldStrategies;
        $this->addFilterStrategies = $addFilterStrategies;
    }

    /**
     * Get the Data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();
        return [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => array_values($items['items']),
        ];
    }

    /**
     * Add Field
     *
     * @param array|string $field Field
     * @param null         $alias Alias
     *
     * @return void
     */
    public function addField($field, $alias = null)
    {
        if (isset($this->addFieldStrategies[$field])) {
            $this->addFieldStrategies[$field]
                ->addField($this->getCollection(), $field, $alias);
        } else {
            parent::addField($field, $alias);
        }
    }

    /**
     * Add Filter
     *
     * @param Filter $filter Filter
     *
     * @return mixed|void
     */
    public function addFilter(Filter $filter)
    {
        if (isset($this->addFilterStrategies[$filter->getField()])) {
            $this->addFilterStrategies[$filter->getField()]
                ->addFilter(
                    $this->getCollection(),
                    $filter->getField(),
                    [$filter->getConditionType() => $filter->getValue()]
                );
        } else {
            parent::addFilter($filter);
        }
    }
}
