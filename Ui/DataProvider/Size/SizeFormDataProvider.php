<?php

namespace Example\SizeGuide\Ui\DataProvider\Size;

use Example\SizeGuide\Model\Resource\Size\Collection;
use Example\SizeGuide\Model\Size;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Example\SizeGuide\Model\Resource\Size\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class SizeFormDataProvider
 *
 * @category DataProvider
 * @package  Example\SizeGuide\Ui\DataProvider\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeFormDataProvider extends AbstractDataProvider
{
    /**
     * Size Collection
     *
     * @var Collection
     */
    protected $collection;

    /**
     * Data Persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Loaded Data
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string                 $name              Name
     * @param string                 $primaryFieldName  Primary Field Name
     * @param string                 $requestFieldName  Request Field Name
     * @param CollectionFactory      $collectionFactory Collection
     * @param DataPersistorInterface $dataPersistor     Data Persistor
     * @param array                  $meta              Meta
     * @param array                  $data              Data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /* @var Size $sizeModel */
        foreach ($items as $sizeModel) {
            $this->loadedData[$sizeModel->getId()] = $sizeModel->getData();
        }

        $data = $this->dataPersistor->get('example_sizeguide_size');

        if (!empty($data)) {
            $sizeModel = $this->collection->getNewEmptyItem();
            $sizeModel->setData($data);
            $this->loadedData[$sizeModel->getId()] = $sizeModel->getData();
            $this->dataPersistor->clear('example_sizeguide_size');
        }

        return $this->loadedData;
    }
}