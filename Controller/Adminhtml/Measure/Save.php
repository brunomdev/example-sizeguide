<?php

namespace Example\SizeGuide\Controller\Adminhtml\Measure;

use Example\SizeGuide\Api\MeasureRepositoryInterface;
use Example\SizeGuide\Model\Measure\ImageUploader;
use Example\SizeGuide\Model\Measure\ImageUploaderPool;
use Example\SizeGuide\Model\MeasureRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class Save
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Measure
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Save extends Action
{
    /**
     * Data Persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Measure Repository
     *
     * @var MeasureRepository|MeasureRepositoryInterface
     */
    protected $measureRepository;

    /**
     * Image Uploader Pool
     *
     * @var ImageUploaderPool
     */
    protected $imageUploaderPool;

    /**
     * Save constructor.
     *
     * @param Context                    $context           Context
     * @param DataPersistorInterface     $dataPersistor     Data Persistor
     * @param LoggerInterface            $logger            Logger
     * @param MeasureRepositoryInterface $measureRepository Measure Repository
     * @param ImageUploaderPool          $imageUploaderPool Image Uploader Pool
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        LoggerInterface $logger,
        MeasureRepositoryInterface $measureRepository,
        ImageUploaderPool $imageUploaderPool
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->logger = $logger;
        $this->measureRepository = $measureRepository;
        $this->imageUploaderPool = $imageUploaderPool;

        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /* @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            /* @var $measureModel \Example\SizeGuide\Model\Measure */
            $measureModel = $this->_objectManager
                ->create('Example\SizeGuide\Model\Measure');

            try {
                if ($id) {
                    $measureModel = $this->measureRepository->getById($id);
                }

                $measureImage = $this->getUploader('image')
                    ->uploadFileAndGetName('image', $data);
                $data['image'] = $measureImage;

                $measureModel->setData($data);
                $this->measureRepository->save($measureModel);

                $this->messageManager->addSuccessMessage(
                    __('You saved the Measure.')
                );
                $this->dataPersistor->clear('example_sizeguide_measure');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit', ['id' => $measureModel->getId()]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());

                $this->messageManager->addExceptionMessage(
                    $e, __('Something went wrong while saving the Measure.')
                );
            }

            $this->dataPersistor->set('example_sizeguide_measure', $data);
            return $resultRedirect->setPath(
                '*/*/edit', ['id' => $this->getRequest()->getParam('id')]
            );
        }

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Get Image Uploader
     *
     * @param string $type Type
     *
     * @return ImageUploader
     *
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->imageUploaderPool->getUploader($type);
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Example_SizeGuide::Measure_save');
    }

}
