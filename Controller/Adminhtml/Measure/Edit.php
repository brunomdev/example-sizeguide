<?php

namespace Example\SizeGuide\Controller\Adminhtml\Measure;

use Example\SizeGuide\Api\MeasureRepositoryInterface;
use Example\SizeGuide\Controller\Adminhtml\Measure;
use Example\SizeGuide\Model\MeasureRepository;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Edit
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Measure
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Edit extends Measure
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Page Factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Measure Repository
     *
     * @var MeasureRepository|MeasureRepositoryInterface
     */
    protected $measureRepository;

    /**
     * Edit constructor.
     *
     * @param Context                    $context           Context
     * @param Registry                   $coreRegistry      Core Registry
     * @param LoggerInterface            $logger            Logger
     * @param PageFactory                $resultPageFactory Page Factory
     * @param MeasureRepositoryInterface $measureRepository Measure Repository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        LoggerInterface $logger,
        PageFactory $resultPageFactory,
        MeasureRepositoryInterface $measureRepository
    ) {
        $this->logger = $logger;
        $this->resultPageFactory = $resultPageFactory;
        $this->measureRepository = $measureRepository;

        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return Page|Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');

        /* @var $measureModel \Example\SizeGuide\Model\Measure */
        $measureModel = $this->_objectManager
            ->create('Example\SizeGuide\Model\Measure');

        // 2. Initial checking
        try {
            if ($id) {
                $measureModel = $this->measureRepository->getById($id);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            $this->messageManager->addErrorMessage(
                __('This Measure no longer exists.')
            );

            /* @var Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/');
        }

        $this->coreRegistry->register('example_sizeguide_measure', $measureModel);

        // 3. Build edit form
        /* @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Measure') : __('New Measure'),
            $id ? __('Edit Measure') : __('New Measure')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Measures'));
        $resultPage->getConfig()->getTitle()->prepend(
            $measureModel->getId() ? $measureModel->getName() : __('New Measure')
        );

        return $resultPage;
    }
}
