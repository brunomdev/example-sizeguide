<?php

namespace Example\SizeGuide\Controller\Adminhtml\Measure;

use Example\SizeGuide\Api\MeasureRepositoryInterface;
use Example\SizeGuide\Model\Measure\ImageUploader;
use Example\SizeGuide\Model\Measure\ImageUploaderPool;
use Example\SizeGuide\Model\MeasureRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class Delete
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Measure
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Delete extends Action
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Measure Repository
     *
     * @var MeasureRepository|MeasureRepositoryInterface
     */
    protected $measureRepository;

    /**
     * Image Uploader Pool
     *
     * @var ImageUploaderPool
     */
    protected $imageUploaderPool;

    /**
     * Save constructor.
     *
     * @param Context                    $context           Context
     * @param LoggerInterface            $logger            Logger
     * @param MeasureRepositoryInterface $measureRepository Measure Repository
     * @param ImageUploaderPool          $imageUploaderPool Image Uploader Pool
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        MeasureRepositoryInterface $measureRepository,
        ImageUploaderPool $imageUploaderPool
    ) {
        $this->logger = $logger;
        $this->measureRepository = $measureRepository;
        $this->imageUploaderPool = $imageUploaderPool;

        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /* @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            try {
                $measure = $this->measureRepository->getById($id);

                $measureImage = $measure->getImage();

                $result = $this->measureRepository->delete($measure);

                if ($result && $measureImage) {
                    $this->getUploader('image')->deleteFile($measureImage);
                }

                // display success message
                $this->messageManager->addSuccessMessage(
                    __('You deleted the Measure.')
                );

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (CouldNotDeleteException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());

                $this->messageManager->addExceptionMessage(
                    $e, __('Something went wrong while deleting the Measure.')
                );
            }

            // go back to edit form
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }

        // display error message
        $this->messageManager->addErrorMessage(
            __('We can\'t find a Measure to delete.')
        );

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Get Image Uploader
     *
     * @param string $type Type
     *
     * @return ImageUploader
     *
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->imageUploaderPool->getUploader($type);
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Example_SizeGuide::Measure_delete'
        );
    }

}
