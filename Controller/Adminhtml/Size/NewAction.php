<?php

namespace Example\SizeGuide\Controller\Adminhtml\Size;

use Example\SizeGuide\Controller\Adminhtml\Size;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Forward;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class NewAction
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class NewAction extends Size
{
    /**
     * Forward Factory
     *
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * NewAction constructor.
     *
     * @param Context        $context              Context
     * @param Registry       $coreRegistry         Core Registry
     * @param ForwardFactory $resultForwardFactory Forward Factory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;

        parent::__construct($context, $coreRegistry);
    }

    /**
     * {@inheritDoc}
     *
     * @return Forward|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $resultForward = $this->resultForwardFactory->create();

        return $resultForward->forward('edit');
    }
}
