<?php

namespace Example\SizeGuide\Controller\Adminhtml\Size;

use Example\SizeGuide\Api\SizeRepositoryInterface;
use Example\SizeGuide\Controller\Adminhtml\Size;
use Example\SizeGuide\Model\SizeRepository;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Edit
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Edit extends Size
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Page Factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Size Repository
     *
     * @var SizeRepository|SizeRepositoryInterface
     */
    protected $sizeRepository;

    /**
     * Edit constructor.
     *
     * @param Context                 $context           Context
     * @param Registry                $coreRegistry      Core Registry
     * @param LoggerInterface         $logger            Logger
     * @param PageFactory             $resultPageFactory Page Factory
     * @param SizeRepositoryInterface $sizeRepository    Size Repository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        LoggerInterface $logger,
        PageFactory $resultPageFactory,
        SizeRepositoryInterface $sizeRepository
    ) {
        $this->logger = $logger;
        $this->resultPageFactory = $resultPageFactory;
        $this->sizeRepository = $sizeRepository;

        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return Page|Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');

        /* @var $sizeModel \Example\SizeGuide\Model\Size */
        $sizeModel = $this->_objectManager->create('Example\SizeGuide\Model\Size');

        // 2. Initial checking
        try {
            if ($id) {
                $sizeModel = $this->sizeRepository->getById($id);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            $this->messageManager->addErrorMessage(
                __('This Size no longer exists.')
            );

            /* @var Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/');
        }

        $this->coreRegistry->register('example_sizeguide_size', $sizeModel);

        // 3. Build edit form
        /* @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Size') : __('New Size'),
            $id ? __('Edit Size') : __('New Size')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Sizes'));
        $resultPage->getConfig()->getTitle()->prepend(
            $sizeModel->getId() ? $sizeModel->getName() : __('New Size')
        );

        return $resultPage;
    }
}
