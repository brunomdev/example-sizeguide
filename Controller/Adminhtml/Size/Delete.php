<?php

namespace Example\SizeGuide\Controller\Adminhtml\Size;

use Example\SizeGuide\Api\SizeRepositoryInterface;
use Example\SizeGuide\Model\SizeRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class Delete
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Delete extends Action
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Size Repository
     *
     * @var SizeRepository|SizeRepositoryInterface
     */
    protected $sizeRepository;

    /**
     * Save constructor.
     *
     * @param Context                 $context        Context
     * @param LoggerInterface         $logger         Logger
     * @param SizeRepositoryInterface $sizeRepository Size Repository
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        SizeRepositoryInterface $sizeRepository
    ) {
        $this->logger = $logger;
        $this->sizeRepository = $sizeRepository;

        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /* @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            try {
                $this->sizeRepository->deleteById($id);

                // display success message
                $this->messageManager->addSuccessMessage(
                    __('You deleted the Size.')
                );

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (CouldNotDeleteException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());

                $this->messageManager->addExceptionMessage(
                    $e, __('Something went wrong while deleting the Size.')
                );
            }

            // go back to edit form
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }

        // display error message
        $this->messageManager->addErrorMessage(
            __('We can\'t find a Size to delete.')
        );

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Example_SizeGuide::Size_delete'
        );
    }

}
