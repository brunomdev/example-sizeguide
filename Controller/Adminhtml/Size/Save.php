<?php

namespace Example\SizeGuide\Controller\Adminhtml\Size;

use Example\SizeGuide\Api\SizeRepositoryInterface;
use Example\SizeGuide\Model\SizeRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class Save
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Save extends Action
{
    /**
     * Data Persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Size Repository
     *
     * @var SizeRepository|SizeRepositoryInterface
     */
    protected $sizeRepository;

    /**
     * Save constructor.
     *
     * @param Context                 $context        Context
     * @param DataPersistorInterface  $dataPersistor  Data Persistor
     * @param LoggerInterface         $logger         Logger
     * @param SizeRepositoryInterface $sizeRepository Size Repository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        LoggerInterface $logger,
        SizeRepositoryInterface $sizeRepository
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->logger = $logger;
        $this->sizeRepository = $sizeRepository;

        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /* @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            /* @var $sizeModel \Example\SizeGuide\Model\Size */
            $sizeModel = $this->_objectManager
                ->create('Example\SizeGuide\Model\Size');

            try {
                if ($id) {
                    $sizeModel = $this->sizeRepository->getById($id);
                }

                $sizeModel->setData($data);
                $this->sizeRepository->save($sizeModel);

                $this->messageManager->addSuccessMessage(__('You saved the Size.'));
                $this->dataPersistor->clear('example_sizeguide_size');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit', ['id' => $sizeModel->getId()]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());

                $this->messageManager->addExceptionMessage(
                    $e, __('Something went wrong while saving the Size.')
                );
            }

            $this->dataPersistor->set('example_sizeguide_size', $data);
            return $resultRedirect->setPath(
                '*/*/edit', ['id' => $this->getRequest()->getParam('id')]
            );
        }

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Example_SizeGuide::Size_save');
    }

}
