<?php

namespace Example\SizeGuide\Controller\Adminhtml\Size;

use Example\SizeGuide\Api\SizeRepositoryInterface;
use Example\SizeGuide\Model\SizeRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class MassDelete
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MassDelete extends Action
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Size Repository
     *
     * @var SizeRepository|SizeRepositoryInterface
     */
    protected $sizeRepository;

    /**
     * Save constructor.
     *
     * @param Context                 $context        Context
     * @param LoggerInterface         $logger         Logger
     * @param SizeRepositoryInterface $sizeRepository Size Repository
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        SizeRepositoryInterface $sizeRepository
    ) {
        $this->logger = $logger;
        $this->sizeRepository = $sizeRepository;

        parent::__construct($context);
    }

    /**
     * Mass delete action
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /* @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // check if we know what should be deleted
        $ids = $this->getRequest()->getParam('selected');

        if ($ids) {
            try {
                $sizesDeleted = 0;
                foreach ($ids as $id) {
                    $this->sizeRepository->deleteById($id);

                    $sizesDeleted++;
                }

                if ($sizesDeleted) {
                    // display success message
                    $this->messageManager->addSuccessMessage(
                        __('A total of %1 record(s) were deleted.', $sizesDeleted)
                    );
                }

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (CouldNotDeleteException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());

                $this->messageManager->addExceptionMessage(
                    $e, __('Something went wrong while deleting the Size.')
                );
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(
            __('We can\'t find a Size to delete.')
        );

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Example_SizeGuide::Size_delete'
        );
    }

}
