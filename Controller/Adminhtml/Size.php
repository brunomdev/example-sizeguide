<?php

namespace Example\SizeGuide\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

/**
 * Class Size
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
abstract class Size extends Action
{
    const ADMIN_RESOURCE = 'Example_SizeGuide::Size';

    /**
     * Core Registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Size constructor.
     *
     * @param Context  $context      Context
     * @param Registry $coreRegistry Core Registry
     */
    public function __construct(Context $context, Registry $coreRegistry)
    {
        $this->coreRegistry = $coreRegistry;

        parent::__construct($context);
    }

    /**
     * Init Page
     *
     * @param Page $resultPage Result Page
     *
     * @return Page
     */
    public function initPage(Page $resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Example'), __('Example'))
            ->addBreadcrumb(__('Sizes'), __('Sizes'));

        return $resultPage;
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}
