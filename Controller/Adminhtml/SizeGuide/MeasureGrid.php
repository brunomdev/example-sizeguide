<?php

namespace Example\SizeGuide\Controller\Adminhtml\SizeGuide;

/**
 * Class MeasureGrid
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\SizeGuide
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasureGrid extends Measure
{

}
