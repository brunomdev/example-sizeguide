<?php

namespace Example\SizeGuide\Controller\Adminhtml\SizeGuide;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResponseInterface;
use Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab\MeasureTable as MeasureTableTab;

/**
 * Class MeasureTable
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasureTable extends Action
{
    /**
     * Result Page Factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * {@inheritdoc}
     *
     * @param Context     $context           Context
     * @param PageFactory $resultPageFactory Result Page Factory
     *
     * @return void
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     *
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        /* @var MeasureTableTab $sizeTableBlock */
        $sizeTableBlock = $resultPage->getLayout()->createBlock(
            'Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab\MeasureTable'
        );

        if ($sizeTableBlock) {
            echo $sizeTableBlock->setTemplate(
                'Example_SizeGuide::sizeguide/measure-table.phtml'
            )->toHtml();
        }

        die;
    }
}
