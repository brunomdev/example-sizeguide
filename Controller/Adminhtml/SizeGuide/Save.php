<?php

namespace Example\SizeGuide\Controller\Adminhtml\SizeGuide;

use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Model\SizeGuideRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Helper\Js;
use Magento\Backend\Model\Session;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class Save
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\SizeGuide
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Save extends Action
{
    /**
     * Data Persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Size Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Save constructor.
     *
     * @param Context                      $context             Context
     * @param DataPersistorInterface       $dataPersistor       Data Persistor
     * @param LoggerInterface              $logger              Logger
     * @param SizeGuideRepositoryInterface $sizeGuideRepository Size Guide Repository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        LoggerInterface $logger,
        SizeGuideRepositoryInterface $sizeGuideRepository
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->logger = $logger;
        $this->sizeGuideRepository = $sizeGuideRepository;

        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /* @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        /* @var Js $jsHelper */
        $jsHelper = $this->_objectManager->create('Magento\Backend\Helper\Js');

        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            /* @var $sizeGuideModel \Example\SizeGuide\Model\SizeGuide */
            $sizeGuideModel = $this->_objectManager
                ->create('Example\SizeGuide\Model\SizeGuide');

            try {
                if ($id) {
                    $sizeGuideModel = $this->sizeGuideRepository->getById($id);
                }

                $sizeGuideModel->setData($data['size']);

                if (isset($data['size_ids'])
                    && ($data['size_ids'] != '' || $data['size_ids'] != null)
                ) {
                    $sizeIds = $jsHelper->decodeGridSerializedInput(
                        $data['size_ids']
                    );

                    $sizeGuideModel->setData('size_ids', $sizeIds);
                }

                if (isset($data['measure_ids'])
                    && ($data['measure_ids'] != '' || $data['measure_ids'] != null)
                ) {
                    $measureIds = $jsHelper->decodeGridSerializedInput(
                        $data['measure_ids']
                    );

                    $sizeGuideModel->setData('measure_ids', $measureIds);
                }

                if (isset($data['size_table'])
                    && ($data['size_table'] != '' || $data['size_table'] != null)
                ) {
                    $sizeTable = serialize(
                        $this->getRequest()->getParam('size_table')
                    );

                    $sizeGuideModel->setData('size_table', $sizeTable);
                }

                if (isset($data['measure_table'])
                    && ($data['measure_table'] != '' || $data['measure_table'] != null)
                ) {
                    $measureTable = serialize(
                        $this->getRequest()->getParam('measure_table')
                    );

                    $sizeGuideModel->setData('measure_table', $measureTable);
                }

                /* @var Session $session */
                $session = $this->_objectManager->get(
                    'Magento\Backend\Model\Session'
                );

                $session->setPageData($sizeGuideModel->getData());

                $this->sizeGuideRepository->save($sizeGuideModel);

                $this->messageManager->addSuccessMessage(
                    __('You saved the Size Guide.')
                );
                $this->dataPersistor->clear('example_sizeguide');

                $session->setPageData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit', ['id' => $sizeGuideModel->getId()]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());

                $this->messageManager->addExceptionMessage(
                    $e, __('Something went wrong while saving the Size Guide.')
                );
            }

            $this->dataPersistor->set('example_sizeguide', $data);
            return $resultRedirect->setPath(
                '*/*/edit', ['id' => $this->getRequest()->getParam('id')]
            );
        }

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Example_SizeGuide::Size Guide_save'
        );
    }

}
