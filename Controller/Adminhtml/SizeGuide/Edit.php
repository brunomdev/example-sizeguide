<?php

namespace Example\SizeGuide\Controller\Adminhtml\SizeGuide;

use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Controller\Adminhtml\SizeGuide;
use Example\SizeGuide\Model\SizeGuideRepository;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Edit
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\Size
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Edit extends SizeGuide
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Page Factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Size Guide Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Edit constructor.
     *
     * @param Context                      $context             Context
     * @param Registry                     $coreRegistry        Core Registry
     * @param LoggerInterface              $logger              Logger
     * @param PageFactory                  $resultPageFactory   Page Factory
     * @param SizeGuideRepositoryInterface $sizeGuideRepository SizeGuide Repository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        LoggerInterface $logger,
        PageFactory $resultPageFactory,
        SizeGuideRepositoryInterface $sizeGuideRepository
    ) {
        $this->logger = $logger;
        $this->resultPageFactory = $resultPageFactory;
        $this->sizeGuideRepository = $sizeGuideRepository;

        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return Page|Redirect|ResponseInterface|ResultInterface|mixed
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');

        /* @var $sizeGuideModel \Example\SizeGuide\Model\SizeGuide */
        $sizeGuideModel = $this->_objectManager->create(
            'Example\SizeGuide\Model\SizeGuide'
        );

        // 2. Initial checking
        try {
            if ($id) {
                $sizeGuideModel = $this->sizeGuideRepository->getById($id);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            $this->messageManager->addErrorMessage(
                __('This Size no longer exists.')
            );

            /* @var Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/');
        }

        $this->coreRegistry->register('example_sizeguide', $sizeGuideModel);

        // 3. Build edit form
        /* @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Size Guide') : __('New Size Guide'),
            $id ? __('Edit Size Guide') : __('New Size Guide')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Size Guide'));
        $resultPage->getConfig()->getTitle()->prepend(
            $sizeGuideModel->getId()
                ? $sizeGuideModel->getName() : __('New Size Guide')
        );

        return $resultPage;
    }
}
