<?php

namespace Example\SizeGuide\Controller\Adminhtml\SizeGuide;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Layout;
use Magento\Framework\App\ResponseInterface;

/**
 * Class Size
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\SizeGuide
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Size extends Action
{
    /**
     * Layout
     *
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * Size constructor.
     *
     * @param Context       $context             Context
     * @param LayoutFactory $resultLayoutFactory Layout Factory
     */
    public function __construct(Context $context, LayoutFactory $resultLayoutFactory)
    {
        $this->resultLayoutFactory = $resultLayoutFactory;

        parent::__construct($context);
    }

    /**
     * {@inheritDoc}
     *
     * @return ResponseInterface|ResultInterface|Layout
     */
    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();

        $resultLayout->getLayout()
            ->getBlock('sizeguide_edit_tab_size')
            ->setSizesRelated(
                $this->getRequest()->getPost('sizes_related', null)
            );

        return $resultLayout;
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Example_SizeGuide::SizeGuide'
        );
    }
}
