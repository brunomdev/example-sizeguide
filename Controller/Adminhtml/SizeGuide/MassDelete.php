<?php

namespace Example\SizeGuide\Controller\Adminhtml\SizeGuide;

use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Model\SizeGuideRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class MassDelete
 *
 * @category Controller
 * @package  Example\SizeGuide\Controller\Adminhtml\SizeGuide
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MassDelete extends Action
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Size Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Save constructor.
     *
     * @param Context                      $context             Context
     * @param LoggerInterface              $logger              Logger
     * @param SizeGuideRepositoryInterface $sizeGuideRepository Size Repository
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        SizeGuideRepositoryInterface $sizeGuideRepository
    ) {
        $this->logger = $logger;
        $this->sizeGuideRepository = $sizeGuideRepository;

        parent::__construct($context);
    }

    /**
     * Mass delete action
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /* @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // check if we know what should be deleted
        $ids = $this->getRequest()->getParam('selected');

        if ($ids) {
            try {
                $sizesGuideDeleted = 0;
                foreach ($ids as $id) {
                    $this->sizeGuideRepository->deleteById($id);

                    $sizesGuideDeleted++;
                }

                if ($sizesGuideDeleted) {
                    // display success message
                    $this->messageManager->addSuccessMessage(
                        __(
                            'A total of %1 record(s) were deleted.',
                            $sizesGuideDeleted
                        )
                    );
                }

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (NoSuchEntityException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (CouldNotDeleteException $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());

                $this->messageManager->addExceptionMessage(
                    $e, __('Something went wrong while deleting the Size Guide.')
                );
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(
            __('We can\'t find a Size Guide to delete.')
        );

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Check if user is allowed
     *
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Example_SizeGuide::SizeGuide_delete'
        );
    }

}
