<?php

namespace Example\SizeGuide\Block\Product;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Model\MeasurePivot;
use Example\SizeGuide\Model\Resource\Measure\Collection as MeasureCollection;
use Example\SizeGuide\Model\Resource\Measure\CollectionFactory as MeasureCollectionFactory;
use Example\SizeGuide\Model\Resource\MeasurePivot\Collection as MeasurePivotCollection;
use Example\SizeGuide\Model\Resource\MeasurePivot\CollectionFactory as MeasurePivotCollectionFactory;
use Example\SizeGuide\Model\Resource\SizePivot\Collection as SizePivotCollection;
use Example\SizeGuide\Model\Resource\SizePivot\CollectionFactory as SizePivotCollectionFactory;
use Example\SizeGuide\Model\SizeGuide as SizeGuideModel;
use Example\SizeGuide\Model\SizeGuideRepository;
use Example\SizeGuide\Model\SizePivot;
use Example\SizeGuide\Model\Source\SizeAttribute;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Config;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class SizeGuide
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Product
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeGuide extends Template
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Size Guide Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Catalog Product
     *
     * @var Product|null
     */
    protected $product = null;

    /**
     * SizeGuide Model
     *
     * @var SizeGuideModel|null
     */
    protected $sizeGuide = null;

    /**
     * Size Pivot Collection
     *
     * @var SizePivotCollection
     */
    protected $sizePivotCollection;

    /**
     * Measure Pivot Collection
     *
     * @var MeasurePivotCollection
     */
    protected $measurePivotCollection;

    /**
     * Measure Collection
     *
     * @var MeasureCollection
     */
    protected $measureCollection;

    /**
     * SizeGuide constructor.
     *
     * @param Context                       $context                       Context
     * @param Registry                      $registry                      Registry
     * @param ObjectManagerInterface        $objectManager                 Object Manager
     * @param SizeGuideRepositoryInterface  $sizeGuideRepository           SizeGuide Repository
     * @param SizePivotCollectionFactory    $sizePivotCollectionFactory    Size Pivot Collection
     * @param MeasurePivotCollectionFactory $measurePivotCollectionFactory Measure Pivot Collection
     * @param MeasureCollectionFactory      $measureCollectionFactory      Measure Collection
     * @param array                         $data                          Data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ObjectManagerInterface $objectManager,
        SizeGuideRepositoryInterface $sizeGuideRepository,
        SizePivotCollectionFactory $sizePivotCollectionFactory,
        MeasurePivotCollectionFactory $measurePivotCollectionFactory,
        MeasureCollectionFactory $measureCollectionFactory,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->objectManager = $objectManager;
        $this->sizeGuideRepository = $sizeGuideRepository;
        $this->sizePivotCollection = $sizePivotCollectionFactory->create();
        $this->measurePivotCollection = $measurePivotCollectionFactory->create();
        $this->measureCollection = $measureCollectionFactory->create();

        parent::__construct($context, $data);
    }

    /**
     * Get the current Product
     *
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->product) {
            $this->product = $this->coreRegistry->registry('product');
        }

        return $this->product;
    }

    /**
     * Get the Product SizeGuide
     *
     * @return SizeGuideInterface|SizeGuideModel|null
     */
    public function getSizeGuide()
    {
        if (!$this->sizeGuide && $this->getProduct()) {
            try {
                $sizeGuideAttribute = $this->getProduct()
                    ->getCustomAttribute('sizeguide_id');

                if ($sizeGuideAttribute) {
                    $this->sizeGuide = $this->sizeGuideRepository->getById(
                        $sizeGuideAttribute->getValue()
                    );
                }
            } catch (\Exception $e) {
                $this->_logger->error($e->getMessage());
            }

        }

        return $this->sizeGuide;
    }

    /**
     * Gets the SizeGuide Size Table
     *
     * @return array|null
     */
    public function getSizeTable()
    {
        $sizeGuide = $this->getSizeGuide();

        if ($sizeGuide && $sizeGuide->getId() && $sizeGuide->getData('size_table')) {
            $columns = [];

            $columns[] = [
                'title' => __('Size'),
                'name' => 'size',
                'value' => 'size'
            ];

            $this->sizePivotCollection->join(
                ['iss' => 'example_sizeguide_size'],
                'main_table.size_id = iss.id',
                'name'
            )
                ->addFieldToFilter(
                    'main_table.sizeguide_id',
                    $sizeGuide->getId()
                )
                ->setOrder('position', 'ASC');

            /* @var SizePivot $sizePivot */
            foreach ($this->sizePivotCollection as $sizePivot) {
                $columns[] = [
                    'title' => $sizePivot->getData('name'),
                    'name' => strtolower($sizePivot->getData('name')),
                    'value' => strtolower($sizePivot->getData('name'))
                ];
            }

            $rows = unserialize($sizeGuide->getData('size_table'));

            $content = [];

            foreach ($columns as $k => $column) {
                foreach ($rows[$column['name']] as $rowKey => $row) {
                    $content[$rowKey][$k] = [
                        'title' => $column['title'],
                        'name' => $column['name'],
                        'value' => $row
                    ];
                }
            }

            return [
                'columns' => $columns,
                'rows' => $content
            ];
        }

        return null;
    }

    /**
     * Gets the SizeGuide Measure Table
     *
     * @return array|null
     */
    public function getMeasureTable()
    {
        try {
            $sizeGuide = $this->getSizeGuide();

            if ($sizeGuide && $sizeGuide->getId()
                && $sizeGuide->getData('measure_table')
            ) {
                $columns = [];

                $columns[] = [
                    'title' => __('Size'),
                    'name' => 'size',
                    'value' => 'size'
                ];

                $this->measurePivotCollection->join(
                    ['ism' => 'example_sizeguide_measure'],
                    'main_table.measure_id = ism.id',
                    'name'
                )
                    ->addFieldToFilter(
                        'main_table.sizeguide_id',
                        $sizeGuide->getId()
                    )
                    ->setOrder('position', 'ASC');

                /* @var MeasurePivot $measurePivot */
                foreach ($this->measurePivotCollection as $measurePivot) {
                    $columns[] = [
                        'title' => $measurePivot->getData('name'),
                        'name' => strtolower($measurePivot->getData('name')),
                        'value' => strtolower($measurePivot->getData('name'))
                    ];
                }

                $rows = unserialize($sizeGuide->getData('measure_table'));

                /* @var Config $eavConfig */
                $eavConfig = $this->objectManager->get('\Magento\Eav\Model\Config');

                $attribute = $eavConfig->getAttribute(
                    'catalog_product',
                    SizeAttribute::ATTRIBUTE_CODE
                );

                $content = [];

                foreach ($columns as $k => $column) {
                    foreach ($rows[$column['name']] as $rowKey => $row) {
                        $value = $row;
                        if ($column['name'] === 'size') {
                            $value = $attribute->getSource()->getOptionText($row);
                        }

                        $content[$rowKey][$k] = [
                            'title' => $column['title'],
                            'name' => $column['name'],
                            'value' => $value
                        ];
                    }
                }

                return [
                    'columns' => $columns,
                    'rows' => $content
                ];
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return null;
    }

    /**
     * Gets the SizeGuide Measures
     *
     * @return MeasureCollection|null
     */
    public function getMeasures()
    {
        $sizeGuide = $this->getSizeGuide();

        if ($sizeGuide && $sizeGuide->getId()) {
            $this->measureCollection->join(
                ['ismp' => 'example_sizeguide_measure_pivot'],
                'main_table.id = ismp.measure_id',
                'position'
            )
                ->addFieldToFilter('ismp.sizeguide_id', $sizeGuide->getId())
                ->setOrder('ismp.position', 'ASC');

            return $this->measureCollection;
        }

        return null;
    }
}
