<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Form
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Form extends Generic
{
    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('sizeguide_form');
        $this->setTitle(__('Size Guide Information'));
    }

    /**
     * Prepares the form
     *
     * @return Generic
     *
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('sizeguide/sizeguide/save'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ],
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
