<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeleteButton
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\SizeGuide\Edit\Buttons
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getSizeGuideId()) {
            $data = [
                'label' => __('Delete Size Guide'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * Get URL for delete button
     *
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', ['id' => $this->getSizeGuideId()]);
    }
}
