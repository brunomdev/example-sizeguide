<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\SizeGuide\Edit\Buttons
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save Size Guide'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }

}
