<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Model\SizeGuide;
use Example\SizeGuide\Model\SizeGuideRepository;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Tabs as WidgetTabs;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Json\EncoderInterface;
use Example\SizeGuide\Model\Resource\SizePivot\CollectionFactory as SizePivotCollectionFactory;
use Example\SizeGuide\Model\Resource\MeasurePivot\CollectionFactory as MeasurePivotCollectionFactory;

/**
 * Class Tabs
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Tabs extends WidgetTabs
{
    /**
     * Size Guide Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Size Pivot Collection
     *
     * @var SizePivotCollectionFactory
     */
    protected $sizePivotCollection;

    /**
     * Measure Pivot Collection
     *
     * @var MeasurePivotCollectionFactory
     */
    protected $measurePivotCollection;

    public function __construct(
        Context $context,
        EncoderInterface $jsonEncoder,
        Session $authSession,
        SizeGuideRepositoryInterface $sizeGuideRepository,
        SizePivotCollectionFactory $sizePivotCollectionFactory,
        MeasurePivotCollectionFactory $measurePivotCollectionFactory,
        array $data = []
    ) {
        $this->sizeGuideRepository = $sizeGuideRepository;
        $this->sizePivotCollection = $sizePivotCollectionFactory->create();
        $this->measurePivotCollection = $measurePivotCollectionFactory->create();

        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }

    /**
     * Internal constructor
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();

        $this->setId('sizeguide_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Size Guide'));
    }

    /**
     * Gets the SizeGuide
     *
     * @return SizeGuideInterface|SizeGuide
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    public function getSizeGuide()
    {
        $id = $this->getRequest()->getParam('id');

        return $this->sizeGuideRepository->getById($id);
    }

    /**
     * Before HTML
     *
     * @return WidgetTabs
     *
     * @throws \Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'size_section',
            [
                'label' => __('Size'),
                'url' => $this->getUrl(
                    'sizeguide/sizeguide/size',
                    ['_current' => true]
                ),
                'class' => 'ajax'
            ]
        );

        $this->addTab(
            'measure_section',
            [
                'label' => __('Measure'),
                'url' => $this->getUrl(
                    'sizeguide/sizeguide/measure',
                    ['_current' => true]
                ),
                'class' => 'ajax'
            ]
        );

        try {
            $sizeGuide = $this->getSizeGuide();
            if ($sizeGuide->getId()) {

                $this->sizePivotCollection
                    ->addFieldToFilter('sizeguide_id', $sizeGuide->getId());
                if ($this->sizePivotCollection->count() > 0) {
                    $this->addTab(
                        'size_table_section',
                        [
                            'label' => __('Size Table'),
                            'url' => $this->getUrl(
                                'sizeguide/sizeguide/sizetable',
                                ['_current' => true]
                            ),
                            'class' => 'ajax'
                        ]
                    );
                }

                $this->measurePivotCollection
                    ->addFieldToFilter('sizeguide_id', $sizeGuide->getId());
                if ($this->measurePivotCollection->count() > 0) {
                    $this->addTab(
                        'measure_table_section',
                        [
                            'label' => __('Measure Table'),
                            'url' => $this->getUrl(
                                'sizeguide/sizeguide/measuretable',
                                ['_current' => true]
                            ),
                            'class' => 'ajax'
                        ]
                    );
                }
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return parent::_beforeToHtml();
    }
}
