<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Model\Resource\MeasurePivot\CollectionFactory;
use Example\SizeGuide\Model\SizeGuide;
use Example\SizeGuide\Model\SizeGuideRepository;
use Example\SizeGuide\Model\MeasurePivot;
use Example\SizeGuide\Model\Source\SizeAttribute;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class MeasureTable
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class MeasureTable extends Template
{
    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Size Guide Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Measure Pivot Collection
     *
     * @var CollectionFactory
     */
    protected $measurePivotCollection;

    /**
     * MeasureTable constructor.
     *
     * @param Context                      $context             Context
     * @param ObjectManagerInterface       $objectManager       Object Manager
     * @param SizeGuideRepositoryInterface $sizeGuideRepository SizeGuideRepository
     * @param CollectionFactory            $collectionFactory   Collection Factory
     * @param array                        $data                Array Data
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        SizeGuideRepositoryInterface $sizeGuideRepository,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->objectManager = $objectManager;
        $this->sizeGuideRepository = $sizeGuideRepository;
        $this->measurePivotCollection = $collectionFactory->create();

        parent::__construct($context, $data);
    }

    /**
     * Gets the SizeGuide
     *
     * @return SizeGuideInterface|SizeGuide
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    public function getSizeGuide()
    {
        $id = $this->getRequest()->getParam('id');

        return $this->sizeGuideRepository->getById($id);
    }

    /**
     * Get the table columns
     *
     * @return array|null
     */
    public function getColumns()
    {
        try {
            $sizeGuide = $this->getSizeGuide();

            if ($sizeGuide && $sizeGuide->getId()) {
                /* @var SizeAttribute $sizeAttribute */
                $sizeAttribute = $this->objectManager->create(SizeAttribute::class);

                $columns[] = [
                    'title' => __('Size'),
                    'name' => 'size',
                    'type' => 'select',
                    'source' => $sizeAttribute->toOptionArray()
                ];

                $this->measurePivotCollection->join(
                    ['ism' => 'example_sizeguide_measure'],
                    'main_table.measure_id = ism.id',
                    'name'
                )
                    ->addFieldToFilter(
                        'main_table.sizeguide_id',
                        $sizeGuide->getId()
                    )
                    ->setOrder('position', 'ASC');

                if ($this->measurePivotCollection->count() < 1) {
                    return null;
                }

                /* @var MeasurePivot $measurePivot */
                foreach ($this->measurePivotCollection as $measurePivot) {
                    $columns[] = [
                        'title' => $measurePivot->getData('name'),
                        'name' => strtolower($measurePivot->getData('name')),
                        'type' => 'text'
                    ];
                }

                return $columns;
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return null;
    }

    /**
     * Return SizeGuide Values if exists
     *
     * @return array|null
     */
    public function getRows()
    {
        try {
            $sizeGuide = $this->getSizeGuide();

            if ($sizeGuide->getId() && $sizeGuide->hasData('measure_table')) {
                $columns = $this->getColumns();
                $rows = unserialize($sizeGuide->getData('measure_table'));

                $result = [];

                foreach ($columns as $k => $column) {
                    foreach ($rows[$column['name']] as $rowKey => $row) {
                        $result[$rowKey][$k] = [
                            'title' => $column['title'],
                            'name' => $column['name'],
                            'type' => $column['type'],
                            'value' => $row
                        ];

                        if ($column['type'] === 'select') {
                            $result[$rowKey][$k]['source'] = $column['source'];
                        }
                    }
                }

                return $result;
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return null;
    }
}
