<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Model\Resource\SizePivot\CollectionFactory;
use Example\SizeGuide\Model\SizeGuide;
use Example\SizeGuide\Model\SizeGuideRepository;
use Example\SizeGuide\Model\SizePivot;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class SizeTable
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class SizeTable extends Template
{
    /**
     * Size Guide Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Size Pivot Collection
     *
     * @var CollectionFactory
     */
    protected $sizePivotCollection;

    /**
     * SizeTable constructor.
     *
     * @param Context                      $context             Context
     * @param SizeGuideRepositoryInterface $sizeGuideRepository SizeGuideRepository
     * @param CollectionFactory            $collectionFactory   Collection Factory
     * @param array                        $data                Array Data
     */
    public function __construct(
        Context $context,
        SizeGuideRepositoryInterface $sizeGuideRepository,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->sizeGuideRepository = $sizeGuideRepository;
        $this->sizePivotCollection = $collectionFactory->create();

        parent::__construct($context, $data);
    }

    /**
     * Gets the SizeGuide
     *
     * @return SizeGuideInterface|SizeGuide
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    public function getSizeGuide()
    {
        $id = $this->getRequest()->getParam('id');

        return $this->sizeGuideRepository->getById($id);
    }

    /**
     * Get the table columns
     *
     * @return array|null
     */
    public function getColumns()
    {
        try {
            $sizeGuide = $this->getSizeGuide();

            if ($sizeGuide && $sizeGuide->getId()) {
                $columns = [];

                $columns[] = [
                    'title' => __('Size'),
                    'name' => 'size',
                    'type' => 'text'
                ];

                $this->sizePivotCollection->join(
                    ['iss' => 'example_sizeguide_size'],
                    'main_table.size_id = iss.id',
                    'name'
                )
                    ->addFieldToFilter(
                        'main_table.sizeguide_id',
                        $sizeGuide->getId()
                    )
                    ->setOrder('position', 'ASC');

                if ($this->sizePivotCollection->count() < 1) {
                    return null;
                }

                /* @var SizePivot $sizePivot */
                foreach ($this->sizePivotCollection as $sizePivot) {
                    $columns[] = [
                        'title' => $sizePivot->getData('name'),
                        'name' => strtolower($sizePivot->getData('name')),
                        'type' => 'text'
                    ];
                }

                return $columns;
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return null;
    }

    /**
     * Return SizeGuide Values if exists
     *
     * @return array|null
     */
    public function getRows()
    {
        try {
            $sizeGuide = $this->getSizeGuide();

            if ($sizeGuide->getId() && $sizeGuide->hasData('size_table')) {
                $columns = $this->getColumns();
                $rows = unserialize($sizeGuide->getData('size_table'));

                $result = [];

                foreach ($columns as $k => $column) {
                    foreach ($rows[$column['name']] as $rowKey => $row) {
                        $result[$rowKey][$k] = [
                            'title' => $column['title'],
                            'name' => $column['name'],
                            'type' => 'text',
                            'value' => $row
                        ];
                    }
                }

                return $result;
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return null;
    }
}
