<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab;

use Example\SizeGuide\Model\SizeGuide;
use Example\SizeGuide\Model\Source\IsActive;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;

/**
 * Class Main
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Main extends Generic implements TabInterface
{
    /**
     * Status Source
     *
     * @var IsActive
     */
    protected $status;

    /**
     * Main constructor.
     *
     * @param Context     $context     Context
     * @param Registry    $registry    Registry
     * @param FormFactory $formFactory Form Factory
     * @param IsActive    $status      Status
     * @param array       $data        Data array
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        IsActive $status,
        array $data = []
    ) {
        $this->status = $status;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Get Tab Label
     *
     * @return Phrase|string
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * Get Tab Title
     *
     * @return Phrase|string
     */
    public function getTabTitle()
    {
        return __('General');
    }

    /**
     * Can show tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is tab hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepares the Form
     *
     * @return Generic
     *
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        /* @var SizeGuide $model */
        $model = $this->_coreRegistry->registry('example_sizeguide');

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('sizeguide_general_');

        $fieldSet = $form->addFieldset(
            'general_fieldset', ['legend' => __('General')]
        );

        if ($model->getId()) {
            $fieldSet->addField(
                'id',
                'hidden',
                ['name' => 'size[id]']
            );
        }

        $fieldSet->addField(
            'name',
            'text',
            [
                'name' => 'size[name]',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true
            ]
        );

        $fieldSet->addField(
            'description',
            'textarea',
            [
                'name' => 'size[description]',
                'label' => __('Description'),
                'title' => __('Description') ,
                'required' => false
            ]
        );

        $fieldSet->addField(
            'is_active',
            'select',
            [
                'name' => 'size[is_active]',
                'label' => __('Enabled'),
                'title' => __('Enabled') ,
                'options' => $this->status->toArray()
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
