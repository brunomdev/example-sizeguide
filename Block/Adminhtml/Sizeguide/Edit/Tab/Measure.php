<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab;

use Example\SizeGuide\Api\Data\SizeGuideInterface;
use Example\SizeGuide\Api\SizeGuideRepositoryInterface;
use Example\SizeGuide\Model\Resource\Measure\Collection;
use Example\SizeGuide\Model\Resource\Measure\CollectionFactory;
use Example\SizeGuide\Model\Resource\MeasurePivot\Collection as MeasurePivotCollection;
use Example\SizeGuide\Model\Resource\MeasurePivot\CollectionFactory as MeasurePivotCollectionFactory;
use Example\SizeGuide\Model\SizeGuide;
use Example\SizeGuide\Model\SizeGuideRepository;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;

/**
 * Class Measure
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Sizeguide\Edit\Tab
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Measure extends Extended
{
    /**
     * Core Registry
     *
     * @var Registry|null
     */
    protected $coreRegistry = null;

    /**
     * Size Guide Repository
     *
     * @var SizeGuideRepository|SizeGuideRepositoryInterface
     */
    protected $sizeGuideRepository;

    /**
     * Size Guide Model
     *
     * @var SizeGuide|null
     */
    protected $sizeGuide = null;

    /**
     * Measure Collection
     *
     * @var Collection
     */
    protected $collection;

    /**
     * Measure Pivot Collection
     *
     * @var MeasurePivotCollection
     */
    protected $measurePivotCollection;

    /**
     * Measure constructor.
     *
     * @param Context                       $context                  Context
     * @param Data                          $backendHelper            Backend Helper
     * @param Registry                      $coreRegistry             Core Registry
     * @param SizeGuideRepositoryInterface  $sizeGuideRepository      SizeGuideRepository
     * @param CollectionFactory             $collectionFactory        Collection Factory
     * @param MeasurePivotCollectionFactory $measureCollectionFactory Collection Factory
     * @param array                         $data                     Data Array
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Registry $coreRegistry,
        SizeGuideRepositoryInterface $sizeGuideRepository,
        CollectionFactory $collectionFactory,
        MeasurePivotCollectionFactory $measureCollectionFactory,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->sizeGuideRepository = $sizeGuideRepository;

        $this->collection = $collectionFactory->create();
        $this->measurePivotCollection = $measureCollectionFactory->create();

        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Internal constructor
     *
     * @return void
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('related_measure_grid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);

        if ($this->getSizeGuide() && $this->getSizeGuide()->getId()) {
            $this->setDefaultFilter(['in_measures' => 1]);
        }
    }

    /**
     * Gets the SizeGuide
     *
     * @return SizeGuideInterface|SizeGuide|null
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    public function getSizeGuide()
    {
        $id = $this->getRequest()->getParam('id');

        if (!$this->sizeGuide && $id) {
            $sizeGuide = $this->sizeGuideRepository->getById($id);

            if ($sizeGuide && $sizeGuide->getId()) {
                $this->sizeGuide = $sizeGuide;
            }
        }

        return $this->sizeGuide;
    }

    /**
     * Prepares the Collection
     *
     * @return Extended
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    protected function _prepareCollection()
    {
        $cond = 'ismp.measure_id = main_table.id AND ismp.sizeguide_id = %d';

        $this->collection->getSelect()->joinLeft(
            ['ismp' => 'example_sizeguide_measure_pivot'],
            sprintf($cond, $this->getSizeGuide()->getId()),
            'position'
        );

        $this->setCollection($this->collection);

        return parent::_prepareCollection();
    }

    /**
     * Add Column filter to Collection
     *
     * @param Column $column Grid Column
     *
     * @return $this|Extended
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_measures') {
            $measureIds = $this->getSelectedMeasures();

            if (empty($measureIds)) {
                $measureIds = 0;
            }

            if ($column->getFilter()->getValue()) {
                $this->getCollection()
                    ->addFieldToFilter('main_table.id', ['in' => $measureIds]);
            } else {
                if ($measureIds) {
                    $this->getCollection()
                        ->addFieldToFilter('main_table.id', ['nin' => $measureIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * Prepares the Columns
     *
     * @return Extended
     *
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_measures',
            [
                'type' => 'checkbox',
                'name' => 'in_measures',
                'values' => $this->getSelectedMeasures(),
                'align' => 'center',
                'index' => 'id',
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );

        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );

        $this->addColumn(
            'tag',
            [
                'header' => __('Tag'),
                'index' => 'tag',
                'header_css_class' => 'col-tag',
                'column_css_class' => 'col-tag'
            ]
        );

        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'name' => 'position',
                'type' => 'number',
                'validate_class' => 'validate-number',
                'index' => 'position',
                'sortable' => false,
                'filter' => false,
                'editable' => true,
                'edit_only' => true,
                'header_css_class' => 'col-position',
                'column_css_class' => 'col-position'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * Return Grid Url
     *
     * @return string
     */
    public function getGridUrl()
    {
        if ($this->hasData('grid_url')) {
            return $this->getData('grid_url');
        }

        return $this->getUrl(
            'sizeguide/sizeguide/measureGrid',
            ['_current' => true]
        );
    }

    /**
     * Return selected Measures
     *
     * @return array
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    protected function getSelectedMeasures()
    {
        $measures = $this->getMeasuresRelated();
        if (!is_array($measures)) {
            $measures = array_keys($this->getSelectedRelatedMeasures());
        }

        return $measures;
    }

    /**
     * Get Selected Measures
     *
     * @return array
     *
     * @throws LocalizedException|NoSuchEntityException
     */
    public function getSelectedRelatedMeasures()
    {
        $measures = [];

        if ($this->getSizeGuide() && $this->getSizeGuide()->getId()) {
            $this->measurePivotCollection->addFieldToFilter(
                'sizeguide_id',
                ['eq' => $this->getSizeGuide()->getId()]
            );
        }

        foreach ($this->measurePivotCollection as $measure) {
            $measures[$measure->getMeasureId()] = [
                'position' => $measure->getPosition()
            ];
        }

        return $measures;
    }
}
