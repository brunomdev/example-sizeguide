<?php

namespace Example\SizeGuide\Block\Adminhtml\Sizeguide;

use Example\SizeGuide\Model\SizeGuide;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;
use Magento\Framework\Phrase;

/**
 * Class Edit
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Sizeguide
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class Edit extends Container
{
    /**
     * Core Registry
     *
     * @var Registry|null
     */
    protected $coreRegistry = null;

    /**
     * Edit constructor.
     *
     * @param Context  $context  Context
     * @param Registry $registry Registry
     * @param array    $data     Data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_sizeguide';
        $this->_blockGroup = 'Example_SizeGuide';

        parent::_construct();

        $this->buttonList->add(
            'save_and_continue_edit',
            [
                'class' => 'save',
                'label' => __('Save and Continue Edit'),
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ]
                    ],
                ]
            ],
            10
        );
    }

    /**
     * Get Header Text
     *
     * @return Phrase|string
     */
    public function getHeaderText()
    {
        /* @var SizeGuide $sizeGuide */
        $sizeGuide = $this->coreRegistry->registry('example_sizeguide');

        if ($sizeGuide->getId()) {
            return __(
                'Edit Size Guide %1',
                $this->escapeHtml($sizeGuide->getName())
            );
        } else {
            return __('New Size Guide');
        }
    }
}
