<?php

namespace Example\SizeGuide\Block\Adminhtml\Measure\Edit\Buttons;

use Magento\Backend\Block\Widget\Context;

/**
 * Class GenericButton
 *
 * @category Block
 * @package  Example\SizeGuide\Block\Adminhtml\Measure\Edit\Buttons
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
abstract class GenericButton
{
    protected $context;

    /**
     * GenericButton constructor.
     *
     * @param Context $context Context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Return model ID
     *
     * @return int|null
     */
    public function getMeasureId()
    {
        return $this->context->getRequest()->getParam('id');
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route  Route
     * @param array  $params Params
     *
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
