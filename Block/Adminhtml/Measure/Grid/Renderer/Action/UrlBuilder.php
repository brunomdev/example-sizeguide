<?php

namespace Example\SizeGuide\Block\Adminhtml\Measure\Grid\Renderer\Action;

use Magento\Framework\UrlInterface;

/**
 * Class UrlBuilder
 *
 * @category Renderer
 * @package  Example\SizeGuide\Block\Adminhtml\Measure\Grid\Renderer\Action
 * @author   Bruno Marcel <bruno.m.dev@gmail.com>
 * @license  NO-LICENSE #
 * @link     http://www.example.com/
 */
class UrlBuilder
{
    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected $frontendUrlBuilder;

    /**
     * UrlBuilder constructor.
     *
     * @param UrlInterface $frontendUrlBuilder Url Interface
     */
    public function __construct(UrlInterface $frontendUrlBuilder)
    {
        $this->frontendUrlBuilder = $frontendUrlBuilder;
    }

    /**
     * Get the URL
     *
     * @param string $routePath Route Path
     * @param mixed  $scope     Scope
     * @param string $store     Store
     *
     * @return string
     */
    public function getUrl($routePath, $scope, $store)
    {
        $this->frontendUrlBuilder->setScope($scope);
        $href = $this->frontendUrlBuilder->getUrl(
            $routePath,
            ['_current' => false, '_query' => '___store=' . $store]
        );

        return $href;
    }
}
